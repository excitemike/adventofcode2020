# If I wanted to use sdl

- This goes in toml

  ```toml
  [dependencies.sdl2]
  version = "0.34"
  default-features = false
  features = ["ttf","image","gfx","mixer","static-link","use-vcpkg"]
  
  [package.metadata.vcpkg]
  dependencies = ["sdl2", "sdl2-image[libjpeg-turbo,tiff,libwebp]", "sdl2-ttf",   "sdl2-gfx", "sdl2-mixer"]
  git = "https://github.com/microsoft/vcpkg"
  rev = "a0518036077baa4"
  
  [package.metadata.vcpkg.target]
  x86_64-pc-windows-msvc = { triplet = "x64-windows-static-md" }
  ```

- add ttf files
- task you can add to tasks.json

  ```json
  {
    "label": "first-time setup",
    "type": "shell",
    "command": "cargo install cargo-vcpkg ; cargo vcpkg build",
    "presentation": {
      "echo": false,
      "reveal": "never",
      "focus": false,
      "panel": "shared",
      "showReuseMessage": false,
      "clear": true
    }
  },
  ```

- example code

  ```rust
  use std::cmp::Ordering;
  
  use sdl2::{
      keyboard::Keycode, pixels::Color, rect::Rect, render::Canvas, ttf::Font,   video::Window, Sdl,
  };
  
  const WINDOW_WIDTH: i32 = 300;
  const WINDOW_HEIGHT: i32 = 700;
  const SPACING: i32 = 6;
  const FONT_SIZE: u16 = 12;
  const ROW_STEP: i32 = FONT_SIZE as i32 + FONT_SIZE as i32 / 6;
  
  struct Vis<'a, 'b> {
      sdl: Sdl,
      canvas: Canvas<Window>,
      font: Font<'a, 'b>,
      first_invalid: Option<usize>,
  }
  
  pub fn main(data: &[usize]) -> Result<(), String> {
      let sdl = sdl2::init()?;
      let mut canvas = sdl
          .video()?
          .window(
              "Merry Christmas 2020!",
              WINDOW_WIDTH as u32,
              WINDOW_HEIGHT as u32,
          )
          .opengl()
          .build()
          .map_err(|e| e.to_string())?
          .into_canvas()
          .build()
          .map_err(|e| e.to_string())?;
      let ttf = sdl2::ttf::init().map_err(|e| e.to_string())?;
      let mut font = ttf.load_font("resources/comicbd.ttf", FONT_SIZE)?;
      font.set_style(sdl2::ttf::FontStyle::BOLD);
  
      canvas.set_draw_color(Color::RGB(50, 50, 50));
      canvas.clear();
      canvas.present();
  
      Vis {
          sdl,
          canvas,
          font,
          first_invalid: None,
      }
      .main_loop(data)?;
  
      Ok(())
  }
  
  fn is_valid(x: usize, xs: &[usize], i: usize) -> bool {
      for (j, a) in xs[..i].iter().enumerate() {
          for b in xs[j + 1..i].iter() {
              if (a != b) && (a + b == x) {
                  return true;
              }
          }
      }
      return false;
  }
  
  // handle the annoying Rect i32
  macro_rules! rect(
      ($x:expr, $y:expr, $w:expr, $h:expr) => (
          Rect::new($x as i32, $y as i32, $w as u32, $h as u32)
      )
  );
  
  impl<'a, 'b> Vis<'a, 'b> {
      fn check_quit(&self) -> bool {
          for event in self
              .sdl
              .event_pump()
              .expect("error pumping events")
              .poll_iter()
          {
              match event {
                  sdl2::event::Event::Quit { .. }
                  | sdl2::event::Event::KeyDown {
                      keycode: Some(Keycode::Escape),
                      ..
                  } => return true,
                  _ => {}
              }
          }
          false
      }
  
      pub fn main_loop(&mut self, data: &[usize]) -> Result<(), String> {
          self.p1(data)?;
          std::thread::sleep(std::time::Duration::new(1, 0));
          //self.p2();
          Ok(())
      }
  
      fn p1(&mut self, data: &[usize]) -> Result<(), String> {
          const OFFSET: usize = 25;
          let mut scroll = 0;
          for (i, x) in data[OFFSET..].iter().enumerate() {
              self.draw_p1(i + OFFSET, data, &mut scroll)?;
              if !is_valid(*x, &data, i + OFFSET) {
                  self.first_invalid = Some(*x);
                  return Ok(());
              }
          }
          Ok(())
      }
  
      fn next_frame(&mut self) -> Result<(), String> {
          if self.check_quit() {
              return Err(String::from("quit"));
          }
          self.canvas.present();
          //std::thread::sleep(std::time::Duration::new(0, 1_000_000));
          self.canvas.clear();
          Ok(())
      }
  
      fn draw_p1(&mut self, current: usize, data: &[usize], scroll: &mut i32)   -> Result<(), String> {
          let mut x = SPACING;
          let mut y = SPACING;
          let mut current_y = 0;
          for (i, number) in data.iter().enumerate() {
              let text = &number.to_string();
              let (width, height) = self.font.size_of(text).map_err(|e| e.  to_string())?;
              if x + width as i32 > WINDOW_WIDTH - SPACING {
                  y += ROW_STEP;
                  x = SPACING;
              }
              if i == current {
                  current_y = y;
              }
              let scrolled_y = y - *scroll;
              if (0 < scrolled_y + ROW_STEP) && (scrolled_y < WINDOW_HEIGHT) {
                  let color = match i.cmp(&current) {
                      Ordering::Less => Color::RGBA(200, 150, 150, 255),
                      Ordering::Equal => Color::RGBA(255, 255, 255, 255),
                      Ordering::Greater => Color::RGBA(150, 150, 150, 255),
                  };
                  let tc = self.canvas.texture_creator();
                  let texture = tc
                      .create_texture_from_surface(self.font.render(text).  blended(color).unwrap())
                      .unwrap();
                  self.canvas
                      .copy(&texture, None, Some(rect![x, scrolled_y, width,   height]))
                      .unwrap();
              }
              x += width as i32 + SPACING;
          }
          let floor = (WINDOW_HEIGHT - ROW_STEP - SPACING) as i32 * 3 / 4;
          *scroll = std::cmp::max(current_y - floor, 0);
          self.next_frame()?;
          Ok(())
      }
  }
  ```
