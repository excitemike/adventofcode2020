# I tried using druid

- but it was super slow???
- Added this to dependencies in toml: `druid = "0.6"`
- Added the following in day11widget.rs

  ```rust
  use druid::{Color, Event, LifeCycle, Rect, RenderContext, Widget};
  use lazy_static::lazy_static;
  use crate::State;
  
  lazy_static! {
  static ref BACKGROUND_COLOR: Color = Color::rgb8(98, 22, 34);
  static ref EMPTY_COLOR: Color = Color::rgb8(211, 57, 67);
  static ref TAKEN_COLOR: Color = Color::rgb8(88, 182, 155);
  static ref FLOOR_COLOR: Color = Color::rgb8(40, 91, 82);
  }
  
  pub struct Day11Widget {
      playing: bool,
  }
  
  impl Day11Widget {
      pub fn new() -> Self {
          Day11Widget {
              playing: false,
          }
      }
  }
  
  impl Widget<State> for Day11Widget {
    fn event(&mut self, _ctx: &mut druid::EventCtx, event: &druid::Event,   data: &mut State, _env: &druid::Env) {
          if let Event::MouseDown(_) = event {
              crate::tick_p1(&mut data.data);
          }
      }
  
    fn lifecycle(&mut self, _ctx: &mut druid::LifeCycleCtx, event: &  druid::LifeCycle, _data: &State, _env: &druid::Env) {
          if let LifeCycle::AnimFrame(_interval) = event {
          }
      }
  
    fn update(&mut self, ctx: &mut druid::UpdateCtx, _old_data: &State,   _data: &State, _env: &druid::Env) {
          if !self.playing {
              // TODO: Update and only redraw changed squares
              ctx.request_paint();
          }
      }
  
    fn layout(&mut self, _ctx: &mut druid::LayoutCtx, bc: &druid::BoxConstraints, data: &State, _env: &druid::Env) ->   druid::Size {
          let max_size = bc.max();
          let data = &data.data;
          let rows = data.len();
          let cols = data[0].len();
          let max_cell_w = max_size.width / cols as f64;
          let max_cell_h = max_size.height / rows as f64;
          let cell_sidelen = max_cell_w.min(max_cell_h);
          druid::Size {
              width: cell_sidelen * cols as f64,
              height: cell_sidelen * rows as f64,
          }
      }
  
    fn paint(&mut self, ctx: &mut druid::PaintCtx, data: &State, _env: &  druid::Env) {
          let data = &data.data;
          let rows = data.len();
          let cols = data[0].len();
          let size = ctx.size();
          let cell_w = size.width / cols as f64;
          let cell_h= size.height / rows as f64;
          /*let font = ctx.text()
              .new_font_by_name("Segoe Print", 8.0)
              .build()
              .unwrap();*/
          for (row_num, row_data) in data.iter().enumerate() {
              for (col_num, cell) in row_data.iter().enumerate() {
                  let x = col_num as f64 * cell_w;
                  let y = row_num as f64 * cell_h;
                let rect = Rect::from_origin_size((x, y), (cell_w - 1.0,  cell_h - 1.0));
                  /*let layout = ctx
                      .text()
                    .new_text_layout(&font, &format!("{}", cell),   std::f64::INFINITY)
                      .build()
                      .unwrap();*/
                  match cell {
                      'L' => {
                          //ctx.draw_text(&layout, (x, y), &*EMPTY_COLOR);
                          ctx.fill(rect, &*EMPTY_COLOR);
                      }
                      '#' => {
                          //ctx.draw_text(&layout, (x, y), &*TAKEN_COLOR);
                          ctx.fill(rect, &*TAKEN_COLOR);
                      }
                      '.' => {
                          //ctx.draw_text(&layout, (x, y), &*FLOOR_COLOR);
                          ctx.fill(rect, &*FLOOR_COLOR);
                      }
                      _ => panic!(),
                  }
              }
          }
      }
  }
  ```

- and the following for vis.rs (main)

  ```rust
  use day11widget::Day11Widget;
  use druid::{AppLauncher, Data, widget::MainAxisAlignment, Widget, WindowDesc, widget::{self, Align, Flex}};
  use itertools::Itertools;
  
  mod day11widget;
  
  #[derive(Clone)]
  pub struct State {
      data: Vec<Vec<char>>
  }
  impl Data for State {
      fn same(&self, other: &Self) -> bool {
          self.data == other.data
      }
  }
  
  pub fn main() {
      let main_window = WindowDesc::new(build_window)
          .title("Day11Vis")
          .window_size((800.0, 800.0));
      
      let initial_state = shared::puzzle_input!()
          .trim_end()
          .split('\n')
          .map(|line| {
              line.chars()
                  .map(|c| match c {
                      'L' => 'L',
                      '.' => '.',
                      _ => panic!(),
                  })
                  .collect_vec()
          })
          .collect_vec();
  
      AppLauncher::with_window(main_window)
          .launch(State { data: initial_state})
          .unwrap();
  }
  
  fn build_window() -> impl Widget<State> {
      let header = widget::Label::new("Day 01");
      let grid = Day11Widget::new();
  
      let layout = Flex::column()
          .must_fill_main_axis(true)
          .main_axis_alignment(MainAxisAlignment::Center)
          .cross_axis_alignment(druid::widget::CrossAxisAlignment::Center)
          .with_spacer(10.0)
          .with_child(header)
          .with_spacer(10.0)
          .with_flex_child(grid, 10.0)
          .with_spacer(10.0);
      Align::centered(layout)
  }
  
  fn get_cell(input: &Vec<Vec<char>>, row: usize, col: usize) -> char {
      match input.get(row) {
          None => ' ',
          Some(row_data) => match row_data.get(col) {
              None => ' ',
              Some(cell) => *cell,
          },
      }
  }
  
  fn adjacent_cells(input: &Vec<Vec<char>>, row: usize, col: usize) -> Vec<char> {
      let mut output = Vec::new();
      if row >= 1 && col >= 1 {
          output.push(get_cell(input, row - 1, col - 1));
      }
      if col >= 1 {
          output.push(get_cell(input, row, col - 1));
      }
      if col >= 1 {
          output.push(get_cell(input, row + 1, col - 1));
      }
      if row >= 1 {
          output.push(get_cell(input, row - 1, col));
      }
      output.push(get_cell(input, row + 1, col));
      if row >= 1 {
          output.push(get_cell(input, row - 1, col + 1));
      }
      output.push(get_cell(input, row, col + 1));
      output.push(get_cell(input, row + 1, col + 1));
      output
  }
  
  pub fn tick_p1(input: &mut Vec<Vec<char>>) -> Vec<(usize, usize, char)>{
      let rows = input.len();
      let cols = input[0].len();
      let mut output: Vec<(usize, usize, char)> = Vec::new();
      for row in 0..rows {
          for col in 0..cols {
              match get_cell(input, row, col) {
                  'L' => {
                      let neighbors = adjacent_cells(input, row, col);
                      if !neighbors.iter().any(|c| c == &'#') {
                          output.push((row, col, '#'));
                      }
                  }
                  '#' => {
                      let neighbors = adjacent_cells(input, row, col);
                      if 4 <= neighbors
                          .iter()
                          .filter(|&c| c == &'#')
                          .count()
                      {
                          output.push((row, col, 'L'));
                      }
                  }
                  '.' => {}
                  _ => panic!(),
              }
          }
      }
      for (row, col, c) in output.iter() {
          set_cell(input, *row, *col, *c);
      }
      output
  }
  
  fn set_cell(input: &mut Vec<Vec<char>>, row: usize, col: usize, value: char) {
      match input.get_mut(row) {
          None => panic!(),
          Some(row_data) => match row_data.get_mut(col) {
              None => panic!(),
              Some(cell) => *cell = value,
          },
      }
  }
  ```
