def main(puzzle_input):
    # ---- day-specific code start
    from math import prod, sqrt

    def parse_tile_block(tb: str):
        lines = tb.splitlines()
        value = int(lines[0][5:9])
        grid = [[c for c in line] for line in lines[1:]]
        left_edge = ''.join(grid[i][0] for i in range(0, 10))
        right_edge = ''.join(grid[i][-1] for i in range(0, 10))
        top_edge = ''.join(grid[0][i] for i in range(0, 10))
        bottom_edge = ''.join(grid[-1][i] for i in range(0, 10))
        return value, left_edge, top_edge, right_edge, bottom_edge, False, 0

    def parse_tile_image(tb: str):
        lines = tb.splitlines()
        value = int(lines[0][5:9])
        image = [line[1:-1] for line in lines[2:-1]]
        return value, image

    def dump_block(block):
        value, left_edge, top_edge, right_edge, bottom_edge, _, _ = block
        print(value)
        print(f'{top_edge}')
        for l, r in zip(left_edge[1:-1], right_edge[1:-1]):
            print(f'{l}        {r}')
        print(f'{bottom_edge}')
    
    def dump_grid(grid, x, y):
        for row in range(y+1):
            print()
            cols = (x + 1) if row == y else 3
            for col in range(cols):
                value, _, _, _, _, is_flipped, num_rotations = grid[row][col]
                print(f'{value} {is_flipped:1} {num_rotations}   ', end='')
            print()
            for col in range(cols):
                value, left_edge, top_edge, right_edge, bottom_edge, _, _ = grid[row][col]
                print(f'{top_edge} ', end='')
            print()
            for subrow in range(1,9):
                for col in range(cols):
                    _, left_edge, _, right_edge, _, _, _ = grid[row][col]
                    print(f'{left_edge[subrow]}        {right_edge[subrow]} ', end='')
                print()
            for col in range(cols):
                value, left_edge, top_edge, right_edge, bottom_edge, _, _, = grid[row][col]
                print(f'{bottom_edge} ', end='')
            print()

    def flip(block):
        value, left_edge, top_edge, right_edge, bottom_edge, is_flipped, num_rotations = block
        return value, right_edge, top_edge[::-1], left_edge, bottom_edge[::-1], not is_flipped, num_rotations

    def rot(block):
        value, left_edge, top_edge, right_edge, bottom_edge, is_flipped, num_rotations = block
        return value, top_edge[::-1], right_edge, bottom_edge[::-1], left_edge, is_flipped, num_rotations+1

    def orientations(block):
        return [b
                for b in (block, flip(block))
                for b in (b, rot(rot(b)))
                for b in (b, rot(b))]

    tile_blocks = puzzle_input.strip().split('\n\n')
    blocks = [parse_tile_block(tb) for tb in tile_blocks]
    tile_images = {k: v for k,v in (parse_tile_image(tb) for tb in tile_blocks)}

    possibilities_by_value = {b[0]: orientations(b)
                              for b in blocks}
    grid_size = int(sqrt(len(blocks)))

    def match_h(left, right):
        _, _, _, right_edge_of_left, _, _, _ = left
        _, left_edge_of_right, _, _, _, _, _ = right
        return right_edge_of_left == left_edge_of_right

    def match_v(top, bottom):
        _, _, _, _, bottom_of_top, _, _ = top
        _, _, top_of_bottom, _, _, _, _ = bottom
        return bottom_of_top == top_of_bottom

    def do_p1(blocks):
        grid = [[None] * grid_size for _ in range(grid_size)]

        def f(x, y, unused_ids):
            matches_for_left_edge =\
                {block for block_id in unused_ids
                       for block in possibilities_by_value[block_id]
                       if match_h(grid[y][x-1], block)}\
                if x > 0 else \
                {b for block_id in unused_ids for b in possibilities_by_value[block_id]}
            matches_for_top_edge =\
                {block for block_id in unused_ids
                       for block in possibilities_by_value[block_id]
                       if match_v(grid[y-1][x], block)}\
                if y > 0 else \
                {b for block_id in unused_ids for b in possibilities_by_value[block_id]}
            matches = matches_for_left_edge.intersection(matches_for_top_edge)
            if not matches:
                return (x,y) == (grid_size-1,grid_size-1)
            for match in matches:
                grid[y][x] = match
                available_ids = unused_ids - {match[0]}

                if x < grid_size-1:
                    if f(x+1, y, available_ids):
                        return True
                elif y < grid_size-1:
                    if f(0, y+1, available_ids):
                        return True
                elif x == y == grid_size-1:
                    return True
            return False
        _ = f(0, 0, set(b[0] for bs in possibilities_by_value.values() for b in bs))
        return prod(grid[x][y][0] for y in (-1, 0) for x in (-1, 0)), grid

    def image_get_xformed(image, x, y, is_flipped, num_rotations):
        w = len(image[0])
        for _ in range(num_rotations):
            x, y = w-1-y, x
        if is_flipped:
            x = w-1-x
        return image[y][x]

    def image_set_xformed(image, x, y, is_flipped, num_rotations):
        w = len(image[0])
        for _ in range(num_rotations):
            x, y = w-1-y, x
        if is_flipped:
            x = w-1-x
        previous = image[y]
        image[y] = previous[:x] + 'O' + previous[x+1:]

    p1, grid = do_p1(blocks)

    haystack = [
        ''.join(
            image_get_xformed(tile_images[value], subcolnum, subrownum, is_flipped, num_rotations)
            for value, _, _, _, _, is_flipped, num_rotations in row
            for subcolnum in range(8)
        )
        for row in grid
        for subrownum in range(8)
    ]

    needle = ["                  # ",
              "#    ##    ##    ###",
              " #  #  #  #  #  #   "]

    def needle_at(x, y, is_flipped, num_rotations):
        for row_offset in range(len(needle)):
            haystack_y = y + row_offset
            needle_row = needle[row_offset]
            for col_offset, needle_char in enumerate(needle_row):
                haystack_x = x + col_offset
                if needle_char == '#':
                    if '#' != image_get_xformed(haystack, haystack_x, haystack_y, is_flipped, num_rotations):
                        return False
        return True

    def has_needle(is_flipped, num_rotations):
        for y in range(1 + len(haystack) - len(needle)):
            for x in range(1 + len(haystack[y]) - len(needle[0])):
                if needle_at(x, y, is_flipped, num_rotations):
                    return True
        return False

    def replace_needle_at(x, y, is_flipped, num_rotations):
        for row_offset in range(len(needle)):
            haystack_y = y + row_offset
            needle_row = needle[row_offset]
            for col_offset, needle_char in enumerate(needle_row):
                if needle_char == '#':
                    haystack_x = x + col_offset
                    image_set_xformed(haystack, haystack_x, haystack_y, is_flipped, num_rotations)
        return True

    def replace_needle(is_flipped, num_rotations):
        for y in range(1 + len(haystack) - len(needle)):
            for x in range(1 + len(haystack[y]) - len(needle[0])):
                if needle_at(x, y, is_flipped, num_rotations):
                    replace_needle_at(x, y, is_flipped, num_rotations)
                    return True
        return False

    is_flipped, num_rotations = next((is_flipped, num_rotations) for is_flipped in [False, True] for num_rotations in range(4) if has_needle(is_flipped, num_rotations))
    while replace_needle(is_flipped, num_rotations):
        pass
    
    p2 = sum(c=='#' for row in haystack for c in row)

    print(f"part 1: {p1}\npart 2: {p2}")
    # ---- day-specific code start


if __name__ == "__main__":
    import os
    import subprocess
    day = int(os.path.split(__file__)[0][-2:])
    main(subprocess.run(
        f'python src/shared/get_puzzle_input.py {day}'.split(), capture_output=True, text=True).stdout)
