#![allow(unused_imports)]
use std::{collections::HashMap, rc::Rc};

use itertools::Itertools;

#[derive(Clone)]
struct Tile {
    value: u16,
    left: String,
    top: String,
    right: String,
    bottom: String,
    flipped: bool,
    rotations: u8,
    image: Rc<Vec<String>>,
}

fn parse_tile(tb: &str) -> Tile {
    let lines = tb.split('\n').collect_vec();
    let value: u16 = lines[0][5..9].parse().unwrap();
    let left = lines
        .iter()
        .skip(1)
        .map(|line| line.chars().next().unwrap())
        .collect::<String>();
    let right = lines
        .iter()
        .skip(1)
        .map(|line| line.chars().rev().next().unwrap())
        .collect::<String>();
    let bottom = lines.iter().rev().next().unwrap().to_string();
    let top = lines[1].to_string();
    let image: Rc<Vec<String>> = lines
        .iter()
        .skip(2)
        .take(8)
        .map(|line| line.chars().skip(1).take(8).collect::<String>())
        .collect_vec()
        .into();
    Tile {
        value,
        left,
        top,
        right,
        bottom,
        flipped: false,
        rotations: 0,
        image,
    }
}

fn flip(t: &Tile) -> Tile {
    assert!(!t.flipped);
    assert_eq!(t.rotations, 0);
    Tile {
        value: t.value,
        left: t.right.clone(),
        top: t.top.chars().rev().collect(),
        right: t.left.clone(),
        bottom: t.bottom.chars().rev().collect(),
        flipped: !t.flipped,
        rotations: 0,
        image: Rc::clone(&t.image),
    }
}

fn rot(t: &Tile) -> Tile {
    assert!(t.rotations < 3);
    Tile {
        value: t.value,
        left: t.top.chars().rev().collect(),
        top: t.right.clone(),
        right: t.bottom.chars().rev().collect(),
        bottom: t.left.clone(),
        flipped: t.flipped,
        rotations: t.rotations + 1,
        image: Rc::clone(&t.image),
    }
}

fn variations(t: Tile) -> Vec<Rc<Tile>> {
    let mut variations = Vec::with_capacity(8);
    variations.push(Rc::new(flip(&t)));
    variations.push(Rc::new(t));
    for i in 0..2 {
        variations.push(Rc::new(rot(&variations[i])));
    }
    for i in 2..4 {
        variations.push(Rc::new(rot(&variations[i])));
    }
    for i in 4..6 {
        variations.push(Rc::new(rot(&variations[i])));
    }
    variations
}

fn match_h(l: &Tile, r: &Tile) -> bool {
    l.right == r.left
}

fn match_v(top: &Tile, bottom: &Tile) -> bool {
    top.bottom == bottom.top
}

struct P1<'a> {
    variations: &'a HashMap<u16, Vec<Rc<Tile>>>,
    grid_size: usize,
    grid: Vec<Vec<Rc<Tile>>>,
}
impl<'a> P1<'a> {
    fn do_it(variations: &'a HashMap<u16, Vec<Rc<Tile>>>) -> (usize, Vec<Vec<Rc<Tile>>>) {
        let placeholder = variations.values().next().unwrap().first().unwrap();
        let grid_size = (variations.len() as f64).sqrt() as usize;
        let mut state = P1 {
            variations,
            grid_size,
            grid: vec![vec![Rc::clone(placeholder); grid_size]; grid_size],
        };
        state.solve(0, 0, &variations.keys().cloned().collect_vec());

        let first_row = state.grid.first().unwrap();
        let last_row = state.grid.last().unwrap();
        let a = first_row.first().unwrap();
        let b = first_row.last().unwrap();
        let c = last_row.first().unwrap();
        let d = last_row.last().unwrap();
        let p1: usize = [a, b, c, d].iter().map(|x| x.value as usize).product();

        state.grid.first().unwrap().first().unwrap();
        (p1, state.grid)
    }
    fn solve(&mut self, x: usize, y: usize, available_ids: &[u16]) -> bool {
        let matches = available_ids
            .iter()
            .flat_map(|id| self.variations[id].iter())
            .filter(|t| (x == 0) || match_h(&self.grid[y][x - 1], t))
            .filter(|t| (y == 0) || match_v(&self.grid[y - 1][x], t))
            .collect_vec();
        for m in matches {
            self.grid[y][x] = Rc::clone(m);
            if x < self.grid_size - 1 {
                let subproblem_available = available_ids
                    .iter()
                    .filter(|&x| x != &m.value)
                    .cloned()
                    .collect_vec();
                if self.solve(x + 1, y, &subproblem_available) {
                    return true;
                }
            } else if y < self.grid_size - 1 {
                let subproblem_available = available_ids
                    .iter()
                    .filter(|&x| x != &m.value)
                    .cloned()
                    .collect_vec();
                if self.solve(0, y + 1, &subproblem_available) {
                    return true;
                }
            }
        }
        (x == y) && (y == (self.grid_size - 1))
    }
}

trait Image {
    fn get_pixel_transformed(&self, x: usize, y: usize, flipped: bool, rotations: u8) -> char;
    fn set_pixel_transformed(&mut self, x: usize, y: usize, flipped: bool, rotations: u8);
}
impl Image for [String] {
    fn get_pixel_transformed(&self, x: usize, y: usize, flipped: bool, rotations: u8) -> char {
        let w = self.len();
        let mut x = x;
        let mut y = y;
        for _ in 0..rotations {
            y = std::mem::replace(&mut x, w - 1 - y);
        }
        if flipped {
            x = w - 1 - x;
        }
        self[y].chars().nth(x).unwrap()
    }
    fn set_pixel_transformed(&mut self, x: usize, y: usize, flipped: bool, rotations: u8) {
        let w = self.len();
        let mut x = x;
        let mut y = y;
        for _ in 0..rotations {
            y = std::mem::replace(&mut x, w - 1 - y);
        }
        if flipped {
            x = w - 1 - x;
        }
        let old = &self[y];
        self[y] = old.chars().take(x).collect::<String>()
            + "O"
            + &old.chars().skip(x + 1).collect::<String>();
    }
}

fn test_mask(
    image: &[String],
    mask: &[&str],
    x: usize,
    y: usize,
    flipped: bool,
    rotations: u8,
) -> bool {
    for (row_offset, mask_row) in mask.iter().enumerate() {
        let image_y = y + row_offset;
        for (col_offset, c) in mask_row.chars().enumerate() {
            let image_x = x + col_offset;
            if (c == '#')
                && ('#' != image.get_pixel_transformed(image_x, image_y, flipped, rotations))
            {
                return false;
            }
        }
    }
    true
}

fn replace_mask_at(
    image: &mut [String],
    mask: &[&str],
    x: usize,
    y: usize,
    flipped: bool,
    rotations: u8,
) {
    for (row_offset, mask_row) in mask.iter().enumerate() {
        let image_y = y + row_offset;
        for (col_offset, c) in mask_row.chars().enumerate() {
            if c == '#' {
                let image_x = x + col_offset;
                image.set_pixel_transformed(image_x, image_y, flipped, rotations);
            }
        }
    }
}

fn replace_mask(image: &mut Vec<String>, mask: &[&str], flipped: bool, rotations: u8) {
    for y in 0..(1 + image.len() - mask.len()) {
        for x in 0..(1 + image[y].len() - mask[0].len()) {
            if test_mask(image, mask, x, y, flipped, rotations) {
                replace_mask_at(image, mask, x, y, flipped, rotations);
                continue;
            }
        }
    }
}

fn p2(grid: Vec<Vec<Rc<Tile>>>) -> usize {
    let mut image = Vec::new();
    for row in grid.iter() {
        for subrow in 0..8 {
            let mut image_row = String::new();
            for t in row {
                for subcol in 0..8 {
                    let c = t
                        .image
                        .get_pixel_transformed(subcol, subrow, t.flipped, t.rotations);
                    image_row.push(c);
                }
            }
            image.push(image_row);
        }
    }
    let mask = [
        "                  # ",
        "#    ##    ##    ###",
        " #  #  #  #  #  #   ",
    ];

    let (flipped, rotations) = [true, false]
        .iter()
        .flat_map(|flipped| (0..3).map(|rotations| (*flipped, rotations)).collect_vec())
        .find(|(flipped, rotations)| {
            // see if the mask is found
            for y in 0..(1 + image.len() - mask.len()) {
                for x in 0..(1 + image[y].len() - mask[0].len()) {
                    if test_mask(&image, &mask, x, y, *flipped, *rotations) {
                        return true;
                    }
                }
            }
            false
        })
        .unwrap();

    replace_mask(&mut image, &mask, flipped, rotations);

    image
        .into_iter()
        .map(|s| s.chars().filter(|&c| c == '#').count())
        .sum()
}

pub fn main() {
    let input = shared::puzzle_input!();
    let tiles = input.trim_end().split("\n\n").map(parse_tile).collect_vec();
    let id_to_variations = tiles
        .into_iter()
        .map(|t| (t.value, variations(t)))
        .collect::<HashMap<_, _>>();

    let (p1, grid) = P1::do_it(&id_to_variations);
    let p2 = p2(grid);

    println!("part 1: {}\npart 2: {}", p1, p2);
}
