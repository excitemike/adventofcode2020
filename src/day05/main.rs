pub fn main() {
    let mut ids = shared::puzzle_input!()
        .trim_end()
        .split('\n')
        .map(|s| {
            s.chars()
                .map(|c| match c {
                    'B' | 'R' => 1,
                    'F' | 'L' => 0,
                    _ => unreachable!("on valid input, won't reach this case"),
                })
                .fold(0, |so_far, digit| so_far * 2 + digit)
        })
        .collect::<Vec<_>>();
    ids.sort_unstable();
    let window_index = ids
        .windows(2)
        .enumerate()
        .find(|(_, v)| if let [a, b] = v { a + 1 != *b } else { false })
        .unwrap()
        .0;
    println!(
        "part 1: {}\npart 2: {}",
        ids.last().unwrap(),
        ids[window_index] + 1
    );
}
