def main(puzzle_input):
    #---- day-specific code start
    from functools import reduce
    letter_to_digit = {'B':1,'F':0,'R':1,'L':0}
    data = [
        reduce(
            lambda a,b: a*2 + letter_to_digit[b],
            line,
            0
        )
        for line in puzzle_input.strip().split('\n')
    ]
    data.sort()
    p2 = next(data[i]+1 for i, (a, b) in enumerate(zip(data, data[1:])) if a+1 != b)
    print(f"part 1: {data[-1]}\npart 2: {p2}")
    #---- day-specific code end

if __name__ == "__main__":
    import os
    import subprocess
    day = int(os.path.split(__file__)[0][-2:])
    main(subprocess.run(f'python src/shared/get_puzzle_input.py {day}'.split(), capture_output=True, text=True).stdout)
