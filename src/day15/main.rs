use itertools::Itertools;
use std::collections::HashMap;

fn f(n: usize) -> usize {
    let input = "8,0,17,4,1,12"
        .split(',')
        .map(|s| s.parse::<usize>().unwrap())
        .collect_vec();
    let mut last_said = *input.last().unwrap();
    let mut dict = (input[..input.len() - 1])
        .iter()
        .enumerate()
        .map(|(i, &n)| (n, i + 1))
        .collect::<HashMap<_, _>>();
    for turn_number in (input.len() + 1)..=n {
        dict.entry(last_said)
            .and_modify(|prev| {
                let what_say = turn_number - 1 - *prev;
                *prev = turn_number - 1;
                last_said = what_say;
            })
            .or_insert_with(|| {
                last_said = 0;
                turn_number - 1
            });
    }
    last_said
}

//use chrono::prelude::Utc;
pub fn main() {
    //let start = Utc::now();
    println!("part 1: {}\npart 2: {}", f(2020), f(30000000));
    //let duration = Utc::now() - start;
    //println!("{}", duration);
}
