def main():
    # ---- day-specific code start
    puzzle_input = "8,0,17,4,1,12"
    numbers = [int(x) for x in puzzle_input.split(',')]
    last_spoken = numbers[-1]
    last_spoken_on = {number : i+1 for (i, number) in enumerate(numbers[:-1])}
    turn_number = len(numbers)+1

    def turn():
        nonlocal turn_number
        nonlocal last_spoken
        #print(f'turn {turn_number}:')
        #print(f'  last spoken is {last_spoken}')

        if last_spoken not in last_spoken_on:
            #print(f'  which is new! saying 0')
            last_spoken_on[last_spoken] = turn_number-1
            last_spoken = 0
        else:
            what_say = turn_number - 1 - last_spoken_on[last_spoken]
            #print(f'  which was last spoken on turn {last_spoken_on[last_spoken]}')
            #print(f'  saying {what_say}')
            last_spoken_on[last_spoken] = turn_number-1
            last_spoken = what_say
        turn_number += 1

    while turn_number <= 2020:
        turn()

    p1 = last_spoken

    puzzle_input = "8,0,17,4,1,12"
    numbers = [int(x) for x in puzzle_input.split(',')]
    last_spoken_on = {number : i+1 for (i, number) in enumerate(numbers[:-1])}
    last_spoken = numbers[-1]
    turn_number = len(numbers)+1
    for turn_number in range(len(numbers)+1, 30000001):
        if last_spoken not in last_spoken_on:
            last_spoken_on[last_spoken] = turn_number-1
            last_spoken = 0
        else:
            what_say = turn_number - 1 - last_spoken_on[last_spoken]
            last_spoken_on[last_spoken] = turn_number-1
            last_spoken = what_say
    p2 = last_spoken

    print(f"part 1: {p1}\npart 2: {p2}")
    # ---- day-specific code end

if __name__ == "__main__":
    main()
