#![allow(unused_imports)]
use std::collections::HashMap;

use itertools::Itertools;

enum Rule {
    Either(Box<Rule>, Box<Rule>),
    One(u8),
    Two(u8, u8),
    Three(u8, u8, u8),
    TerminalA,
    TerminalB,
}

fn parse_rule(line: &str) -> Rule {
    if let Some((a, b)) = line.split(" | ").collect_tuple() {
        return Rule::Either(Box::new(parse_rule(a)), Box::new(parse_rule(b)));
    }
    match &line.split(' ').collect_vec()[..] {
        [a, b, c] => Rule::Three(a.parse().unwrap(), b.parse().unwrap(), c.parse().unwrap()),
        [a, b] => Rule::Two(a.parse().unwrap(), b.parse().unwrap()),
        ["\"a\""] => Rule::TerminalA,
        ["\"b\""] => Rule::TerminalB,
        [a] => Rule::One(a.parse().unwrap()),
        _ => panic!(),
    }
}

fn rule_id_matches<'a>(rules: &HashMap<u8, Rule>, msg: &'a str, rule_id: u8) -> Vec<&'a str> {
    rule_matches(rules, msg, rules.get(&rule_id).unwrap())
}

fn rule_matches<'a>(rules: &HashMap<u8, Rule>, msg: &'a str, rule: &Rule) -> Vec<&'a str> {
    if msg.is_empty() {
        return Vec::new();
    }
    match rule {
        Rule::Either(a, b) => {
            let mut v = rule_matches(rules, msg, a);
            v.extend(rule_matches(rules, msg, b).iter());
            v
        }
        Rule::One(a) => rule_id_matches(rules, msg, *a),
        Rule::Two(a, b) => rule_id_matches(rules, msg, *a)
            .iter()
            .flat_map(|remaining| rule_id_matches(rules, remaining, *b))
            .collect_vec(),
        Rule::Three(a, b, c) => rule_id_matches(rules, msg, *a)
            .iter()
            .flat_map(|remaining| rule_id_matches(rules, remaining, *b))
            .flat_map(|remaining| rule_id_matches(rules, remaining, *c))
            .collect_vec(),
        Rule::TerminalA => {
            let c = msg.chars().next().unwrap();
            if c == 'a' {
                vec![&msg[c.len_utf8()..]]
            } else {
                Vec::new()
            }
        }
        Rule::TerminalB => {
            let c = msg.chars().next().unwrap();
            if c == 'b' {
                vec![&msg[c.len_utf8()..]]
            } else {
                Vec::new()
            }
        }
    }
}

pub fn main() {
    let input = shared::puzzle_input!();
    let (rules_block, messages_block) = input.split("\n\n").collect_tuple().unwrap();
    let received_messages = messages_block.split('\n').collect_vec();
    let mut rules: HashMap<u8, Rule> = rules_block
        .split('\n')
        .map(|line| {
            let (rule_id, rule) = line.split(": ").collect_tuple().unwrap();
            let rule_id = rule_id.parse::<u8>().unwrap();
            let rule = parse_rule(rule);
            (rule_id, rule)
        })
        .collect();

    let p1: u64 = received_messages
        .iter()
        .cloned()
        .map(|msg| rule_id_matches(&rules, msg, 0).iter().any(|s| s.is_empty()) as u64)
        .sum();

    *rules.get_mut(&8).unwrap() = Rule::Either(Box::new(Rule::One(42)), Box::new(Rule::Two(42, 8)));
    *rules.get_mut(&11).unwrap() = Rule::Either(
        Box::new(Rule::Two(42, 31)),
        Box::new(Rule::Three(42, 11, 31)),
    );

    let p2: u64 = received_messages
        .iter()
        .cloned()
        .map(|msg| rule_id_matches(&rules, msg, 0).iter().any(|s| s.is_empty()) as u64)
        .sum();

    println!("part 1: {}\npart 2: {}", p1, p2);
}
