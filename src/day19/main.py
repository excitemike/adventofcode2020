def main(puzzle_input):
    # ---- day-specific code start
    def valid_messages(rules, initial_state, is_p2):
        import pickle
        fname = f'day19_cache_p2_{initial_state}' if is_p2 else f'day19_cache_p1_{initial_state}'
        try:
            return pickle.load(open(fname, 'rb'))
        except:
            unfinished_states = {('0')}
            finished_states = set()
            while unfinished_states:
                state = unfinished_states.pop()
                i, rule_id = next((i, item) for i, item in enumerate(state) if item.isdigit())
                for production_option in rules[rule_id]:
                    new_state = list(state)
                    new_state[i:i+1] = production_option
                    if len(new_state) < 97:
                        if any(x.isdigit() for x in new_state):
                            unfinished_states.add(tuple(new_state))
                        else:
                            finished_states.add(''.join(tuple(new_state)))
            pickle.dump(frozenset(finished_states), open(fname, 'wb'))
            return finished_states

    rules_block, messages_block = puzzle_input.strip().split('\n\n')
    received_messages = messages_block.splitlines()
    rules = {}
    for rule in rules_block.splitlines():
        rule_id, output = rule.split(': ')
        rules[rule_id] = [ [subrule[1:-1]] if '"' in subrule else [rule_id
                                                                for rule_id in subrule.split(' ')
                                                                if rule_id]
                            for subrule in output.split('|')]
    valid_messages_p1 = valid_messages(rules, 0, False)

    received_valid_messages_p1 = valid_messages_p1.intersection(received_messages)
    p1 = len(received_valid_messages_p1)

    rules['8'] = [['42'], ['42', '8']]
    rules['11'] = [['42', '31'], ['42', '11', '31']]

    def rule_matches(msg: str, rule_id):
        if not msg:
            return []
        if rule_id.isalpha():
            if rule_id==msg[0]:
                #print(f'{rule_id}, {msg} --> terminal match')
                return [msg[1:]]
            #print(f'{rule_id}, {msg} --> terminal mismatch')
            return []
        results = [result 
            for id_list in rules[rule_id]
            for result in rule_list_matches(msg, id_list)]
        #print(f'{rule_id}, {msg} --> {results}')
        return results

    def rule_list_matches(msg: str, id_list):
        remaining = msg
        rule_id, *more_ids = id_list
        results = []
        for remaining in rule_matches(msg, rule_id):
            if more_ids:
                #print(f'{id_list}, {msg} --> {more_ids} {remaining}')
                results.extend(rule_list_matches(remaining, more_ids))
            else:
                #print(f'{id_list}, {msg} --> {remaining}')
                results.append(remaining)
        return results

    def test_message(msg):
        return any(s=='' for s in rule_matches(msg, '0'))

    p2 = sum(1 for msg in received_messages if test_message(msg))
    print(f"part 1: {p1}\npart 2: {p2}")
    # ---- day-specific code start

if __name__ == "__main__":
    import os
    import subprocess
    day = int(os.path.split(__file__)[0][-2:])
    main(subprocess.run(f'python src/shared/get_puzzle_input.py {day}'.split(), capture_output=True, text=True).stdout)
