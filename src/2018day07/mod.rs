#![allow(unused_imports)]
use crate::puzzle_input;
use std::cmp::Ordering::*;
use std::cmp::max;
use std::collections::HashMap;

fn dependencies() -> Vec<(char, char)> {
    puzzle_input!()
        .split('\n')
        .map(|line| {
            let mut words = line
                .trim()
                .split(' ')
                .filter(|s| s.len() == 1);
            let a = words.next().unwrap().chars().next().unwrap();
            let b = words.next().unwrap().chars().next().unwrap();
            (a, b)
        })
        .collect()
}

fn get_steps(dependencies: &[(char, char)]) -> Vec<char> {
    let mut steps = Vec::new();
    for (a, b) in dependencies {
        if !steps.contains(a) {
            steps.push(*a);
        }
        if !steps.contains(b) {
            steps.push(*b);
        }
    }
    steps.sort();
    steps
}

fn blocked(
    dependencies: &[(char, char)],
    ordering_so_far: &[char],
    step: char
) -> bool
{
    for (need, result) in dependencies {
        if (*result == step) && (!ordering_so_far.contains(need)) {
            return true
        }
    }
    false
}

fn determine_order(
    dependencies: &Vec<(char, char)>,
    all_steps: &Vec<char>
) -> Vec<char>
{
    let mut ordering = Vec::new();
    
    while ordering.len() < all_steps.len() {
        for step in all_steps {
            if !ordering.contains(step) {
                if !blocked(&dependencies, &ordering[..], *step) {
                    ordering.push(*step);
                    break
                }
            }
        }
    }
    ordering
}

fn to_time(step: char) -> usize {
    (step as usize) + 61 - ('A' as usize)
}

fn time_needed(
    dependencies: &Vec<(char, char)>,
    all_steps: &Vec<char>
) -> usize {
    let mut cur_time = 0;
    let mut workers:[Option<(usize, char)>; 5] = [None; 5];
    
    let mut available = all_steps.clone();
    let mut completed = Vec::new();
    while !available.is_empty() {
        // for workers that have finished, record output and mark as available
        for worker in workers.iter_mut() {
            if let Some((end_time, product)) = worker {
                if end_time <= &mut cur_time {
                    completed.push(*product);
                    eprintln!("{}", completed.iter().collect::<String>());
                    *worker = None;
                }
            }
        }
        
        // find work for available workers
        for worker in workers.iter_mut() {
            if worker.is_none() {
                for (i, step) in available.iter().enumerate() {
                    if !blocked(&dependencies, &completed[..], *step) {
                        let step = *step;
                        available.remove(i);
                        let end_time = cur_time + to_time(step);
                        *worker = Some((end_time, step));
                        break
                    }
                }
            }
        }
        
        // move clock ahead
        let mut next_finish_time = None;
        for worker in &workers {
            if let Some((candidate_time, ..)) = worker {
                match next_finish_time {
                    None => next_finish_time = Some(candidate_time),
                    Some(current_best) if candidate_time < current_best => next_finish_time = Some(candidate_time),
                    _ => (),
                }
            }
        }
        
        cur_time = *next_finish_time.unwrap();
    }
    workers
        .iter()
        .fold(
            0,
            |t, w| match w {
                Some((end_time,..)) => max(*end_time, t),
                _ => t,
            }
        )
}

pub fn main () -> Result<String, std::io::Error> {
    let dependencies = dependencies();
    let steps = get_steps(&dependencies);
    let ordering = determine_order(&dependencies, &steps);
    let p1 = ordering.iter().collect::<String>();
    let p2 = time_needed(&dependencies, &steps);
    
    Ok(format!("part 1: {}\npart 2: {}", p1, p2))
}
