use itertools::Itertools;

fn remove_next(head: usize, map: &mut [Option<u32>]) -> u32 {
    let next = map[head].unwrap();
    // note the .take()
    let nextnext = map[next as usize].take().unwrap();
    map[head].replace(nextnext);
    next
}

fn insert_cup_after(old: usize, new: u32, next: &mut [Option<u32>]) {
    assert!(old != new as usize, "old: {}, new: {}", old, new);
    next[new as usize] = next[old];
    next[old] = Some(new);
}

fn pick_up_cups(head: usize, map: &mut [Option<u32>]) -> [u32; 3] {
    [
        remove_next(head, map),
        remove_next(head, map),
        remove_next(head, map),
    ]
}

fn get_destination_cup(current_cup: u32, removed_cups: &[u32; 3], num_cups: u32) -> u32 {
    let mut dst_cup = 1 + (current_cup + num_cups - 2) % num_cups;
    let removed_values = removed_cups.iter().cloned().collect_vec();

    // skip cups that have been picked up
    loop {
        if dst_cup == 0 {
            dst_cup = num_cups;
        } else if removed_values.contains(&dst_cup) {
            dst_cup = 1 + (dst_cup + num_cups - 2) % num_cups;
        } else {
            break;
        }
    }
    assert!(
        current_cup != dst_cup,
        "cur_cup: {}, dst_cup: {}",
        current_cup,
        dst_cup
    );
    dst_cup
}

fn do_turn(head: &mut usize, tail: &mut usize, map: &mut [Option<u32>], num_cups: u32) {
    // pick up 3 cups
    let removed_cups = pick_up_cups(*head, map);
    // find where they go
    let dst_cup = get_destination_cup(*head as u32, &removed_cups, num_cups);
    // insert picked up cups
    for cup in removed_cups.iter().rev() {
        assert!(
            dst_cup != *cup,
            "inserting picked up cup: dst_cup: {}, cup: {}",
            dst_cup,
            cup
        );
        insert_cup_after(dst_cup as usize, *cup, map);
    }
    // move to next
    let old_head = *head;
    *head = map[*head].unwrap() as usize;
    // update tail
    while let Some(x) = map[*tail] {
        *tail = x as usize;
    }
    insert_cup_after(*tail, old_head as u32, map);
    *tail = old_head;
}

fn run_game(input: &str, num_cups: u32, num_turns: u32) -> Vec<usize> {
    let mut head = input.chars().next().unwrap().to_digit(10).unwrap() as usize;
    let mut tail = num_cups as usize;
    let mut next = vec![None; (num_cups + 1) as usize];
    {
        let mut prev = 0;
        for c in input.trim_end().chars() {
            let cup_val = c.to_digit(10).unwrap();
            if prev == 0 {
                head = cup_val as usize;
            } else {
                next[prev] = Some(cup_val);
            }
            tail = cup_val as usize;
            prev = cup_val as usize;
        }
        for cup_val in 10..=num_cups {
            next[prev] = Some(cup_val);
            tail = cup_val as usize;
            prev = cup_val as usize;
        }
    }

    for _ in 0..num_turns {
        do_turn(&mut head, &mut tail, &mut next, num_cups);
    }

    let mut ret_val = Vec::with_capacity(8);
    let mut node = next[1].unwrap() as usize;
    for _ in 0..8 {
        ret_val.push(node);
        match next[node] {
            Some(next) => node = next as usize,
            _ => node = head,
        }
    }

    ret_val
}

pub fn main() {
    let input = shared::puzzle_input!();
    let p1 = run_game(&input[..], 9, 100)
        .iter()
        .fold(0, |acc, digit| acc * 10 + digit);
    let p2: usize = run_game(&input[..], 1000000, 10000000)
        .iter()
        .take(2)
        .product();

    println!("part 1: {}\npart 2: {}", p1, p2);
}
