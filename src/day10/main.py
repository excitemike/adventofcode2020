def main(puzzle_input):
    # ---- day-specific code start
    numbers = [int(line) for line in puzzle_input.strip().splitlines()]
    numbers.append(0)
    numbers.append(max(*numbers) + 3)
    numbers.sort()
    deltas = [b-a for a,b in zip(numbers, numbers[1:])]
    one_jolt_diffs = sum(1 for x in deltas if x == 1)
    three_jolt_diffs = sum(1 for x in deltas if x == 3)
    p1 = one_jolt_diffs * three_jolt_diffs

    p2 = 1
    current_run = 0
    lookup = [1,1,2,4,7]
    for delta in deltas:
        if delta == 1:
            current_run += 1
        elif delta == 3:
            p2 *= lookup[current_run]
            current_run = 0
        else:
            raise ":("
    p2 *= lookup[current_run]

    print(f"part 1: {p1}\npart 2: {p2}")
    # ---- day-specific code end

if __name__ == "__main__":
    import os
    import subprocess
    day = int(os.path.split(__file__)[0][-2:])
    main(subprocess.run(f'python src/shared/get_puzzle_input.py {day}'.split(), capture_output=True, text=True).stdout)
