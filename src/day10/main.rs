use itertools::Itertools;
use std::collections::HashMap;

const GAP_MAX: usize = 3;

pub fn main() {
    let puzzle_input = shared::puzzle_input!();
    let mut adapters = puzzle_input
        .trim_end()
        .split('\n')
        .map(|x| x.parse::<usize>().unwrap())
        .collect::<Vec<_>>();
    adapters.push(0);
    let built_in_adapter: usize = adapters.iter().max().unwrap() + 3;
    adapters.push(built_in_adapter);
    adapters.sort_unstable();

    let mut one_jolt_diffs = 0;
    let mut three_jolt_diffs = 0;
    for pair in adapters.windows(2) {
        if let [a, b] = *pair {
            match b - a {
                1 => one_jolt_diffs += 1,
                3 => three_jolt_diffs += 1,
                _ => {}
            }
        } else {
            panic!();
        }
    }
    let mut steps = adapters
        .windows(2)
        .map(|pair| {
            if let [a, b] = *pair {
                b - a
            } else {
                panic!();
            }
        })
        .collect_vec();

    let p2 = memoized(&mut steps);
    assert_eq!(smart_way(&steps), p2);

    println!(
        "part 1: {}\npart 2: {}",
        one_jolt_diffs * three_jolt_diffs,
        p2
    );
}

fn smart_way(steps: &[usize]) -> usize {
    // this only works if the step between adapters is only ever 1 or 3
    for step in steps {
        assert!(step == &1 || step == &3);
    }
    let mut runs_of_ones = vec![0];
    for step in steps {
        match step {
            1 => *runs_of_ones.last_mut().unwrap() += 1,
            3 => runs_of_ones.push(0),
            _ => panic!(),
        }
    }
    runs_of_ones
        .iter()
        .map(|n| match n {
            0 => 1,
            1 => 1,
            2 => 2,
            3 => 4,
            4 => 7,
            _ => panic!(),
        })
        .product::<usize>()
}

fn memoized(steps: &mut [usize]) -> usize {
    let mut memos = HashMap::new();
    subproblem(steps, 0, &mut memos)
}

fn subproblem(
    sorted_gaps: &mut [usize],
    i: usize,
    memos: &mut HashMap<(usize, Option<usize>), usize>,
) -> usize {
    // even though we mutate the list, we don't modify anything after i, so
    // it's sufficient to key on (index, first element) instead of the whole input slice
    let key = (i, sorted_gaps.first().cloned());
    match memos.get(&key) {
        Some(result) => *result,
        None => {
            let result = match *sorted_gaps {
                // end point
                [crate::GAP_MAX] => 1,

                // 3-step can never be removed
                [crate::GAP_MAX, ..] => subproblem(&mut sorted_gaps[1..], i + 1, memos),

                // higher step is invalid
                [a, ..] if a > GAP_MAX => 0,

                // removing is effectively the same as increasing the next gap
                [a, ..] => {
                    let keep = subproblem(&mut sorted_gaps[1..], i + 1, memos);
                    sorted_gaps[1] += a;
                    let skip = subproblem(&mut sorted_gaps[1..], i + 1, memos);
                    sorted_gaps[1] -= a;
                    keep + skip
                }

                _ => panic!(),
            };
            memos.insert(key, result);
            result
        }
    }
}
