def main(puzzle_input):
    # pylint: disable=unused-variable
    def dump(data, bounds):
        minx, miny, minz, minw, maxx, maxy, maxz, maxw = bounds
        print(f'######')
        for w in range(minw, maxw+1):
            for z in range(minz, maxz+1):
                print(f'\nz={z}, w={w}')
                for y in range(miny, maxy+1):
                    for x in range(minx, maxx+1):
                        if (x, y, z, w) in data:
                            print('#', end='')
                        else:
                            print('.', end='')
                    print()

    # ---- day-specific solution code start
    puzzle_input = [[c for c in line] for line in puzzle_input.strip().splitlines()]

    def count_active_neighbors_3d(input_state, in_x, in_y, in_z, in_w):
        me = (in_x, in_y, in_z, in_w)
        w = in_w
        return sum(1 for z in range(in_z-1, in_z+2)
                   for y in range(in_y-1, in_y+2)
                   for x in range(in_x-1, in_x+2)
                   if (x, y, z, w) != me and (x, y, z, w) in input_state)

    def count_active_neighbors_4d(input_state, in_x, in_y, in_z, in_w):
        me = (in_x, in_y, in_z, in_w)
        return sum(1 for w in range(in_w-1, in_w+2)
                   for z in range(in_z-1, in_z+2)
                   for y in range(in_y-1, in_y+2)
                   for x in range(in_x-1, in_x+2)
                   if (x, y, z, w) != me and (x, y, z, w) in input_state)

    def cell_logic(input_state, in_x, in_y, in_z, in_w, num_active_neighbors):
        if (in_x, in_y, in_z, in_w) in input_state:
            return num_active_neighbors in (2, 3)
        return num_active_neighbors == 3

    def step(input_state, bounds, active_neighbors_fn):
        minx, miny, minz, minw, maxx, maxy, maxz, maxw = bounds
        output_state = {(x, y, z, w)
                        for w in range(minw-1, maxw+2)
                        for z in range(minz-1, maxz+2)
                        for y in range(miny-1, maxy+2)
                        for x in range(minx-1, maxx+2)
                        if cell_logic(input_state, x, y, z, w, active_neighbors_fn(input_state, x, y, z, w))}
        for (x, y, z, w) in output_state:
            minx = min(minx, x)
            miny = min(miny, y)
            minz = min(minz, z)
            minw = min(minw, w)
            maxx = max(maxx, x)
            maxy = max(maxy, y)
            maxz = max(maxz, z)
            maxw = max(maxw, w)
        return output_state, (minx, miny, minz, minw, maxx, maxy, maxz, maxw)

    def f(count_active_neighbors_fn):
        bounds = 0, 0, 0, 0, len(puzzle_input[0])-1, len(puzzle_input)-1, 0, 0
        data = {(x, y, 0, 0)
                for y, line in enumerate(puzzle_input)
                for x, c in enumerate(line)
                if c == '#'}
        #dump(data, bounds)
        for i in range(6):
            #print(f'after {i+1} cycles')
            data, bounds = step(data, bounds, count_active_neighbors_fn)
            #dump(data, bounds)
        return len(data)

    print(
        f"part 1: {f(count_active_neighbors_3d)}\npart 2: {f(count_active_neighbors_4d)}")
    # ---- day-specific solution code start

if __name__ == "__main__":
    import os
    import subprocess
    day = int(os.path.split(__file__)[0][-2:])
    main(subprocess.run(f'python src/shared/get_puzzle_input.py {day}'.split(), capture_output=True, text=True).stdout)
