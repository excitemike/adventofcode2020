#![allow(unused_imports)]
use itertools::Itertools;

const ROW_BITS: u32 = 22;

fn step(input: &[Vec<Vec<u32>>], output: &mut Vec<Vec<Vec<u32>>>, is_4d: bool) {
    use std::cmp::{max, min};
    let max_end_w = output.len();
    for (w, zyx) in output.iter_mut().enumerate() {
        let in_ws = if is_4d {
            max(w, 1) - 1..min(w + 2, max_end_w)
        } else {
            w..w + 1
        };
        for (z, yx) in zyx.iter_mut().enumerate() {
            for (y, row) in yx.iter_mut().enumerate() {
                for x in 0..ROW_BITS {
                    let mut active_neighbors_plus_self = 0;
                    for in_w in in_ws.clone() {
                        let in_zyxs = &input[in_w];
                        for in_yxs in in_zyxs
                            .iter()
                            .take(min(z + 2, in_zyxs.len()))
                            .skip(max(z, 1) - 1)
                        {
                            for row in in_yxs
                                .iter()
                                .take(min(y + 2, in_yxs.len()))
                                .skip(max(y, 1) - 1)
                            {
                                for in_x in max(x, 1) - 1..min(x + 2, ROW_BITS) {
                                    if 0 != row & (1 << in_x) {
                                        active_neighbors_plus_self += 1;
                                    }
                                }
                            }
                        }
                    }
                    let mask = 1u32 << x;
                    let active = 0 != input[w][z][y] & mask;
                    if active {
                        if [3, 4].contains(&active_neighbors_plus_self) {
                            *row |= mask;
                        } else {
                            *row &= !mask;
                        }
                    } else if active_neighbors_plus_self == 3 {
                        *row |= mask;
                    } else {
                        *row &= !mask;
                    }
                }
            }
        }
    }
}

pub fn main() {
    let input = shared::puzzle_input!();
    let parsed_input = input
        .split('\n')
        .map(|line| {
            line.chars().fold(
                0u32,
                |acc, c| if c == '#' { (acc << 1) | 1 } else { acc << 1 },
            )
        })
        .collect_vec();
    let nsteps = 6;
    let margin = nsteps;
    let ysize = parsed_input.len();
    let zsize = 1 + 2 * margin;
    let wsize = 1 + 2 * margin;
    let part_fn = |is_4d| {
        let mut buffer_a = vec![vec![vec![0u32; ysize + 2 * margin]; zsize]; wsize];
        let mut buffer_b = vec![vec![vec![0u32; ysize + 2 * margin]; zsize]; wsize];
        for (y, row) in parsed_input.iter().enumerate() {
            buffer_a[margin][margin][y + margin] |= row << margin;
        }
        for i in 0..nsteps {
            if i % 2 == 0 {
                step(&buffer_a, &mut buffer_b, is_4d);
            } else {
                step(&buffer_b, &mut buffer_a, is_4d)
            }
        }
        let final_buf = if nsteps % 2 == 0 { buffer_a } else { buffer_b };
        final_buf
            .iter()
            .map(|zyx| {
                zyx.iter()
                    .map(|yx| yx.iter().map(|x| x.count_ones()).sum::<u32>())
                    .sum::<u32>()
            })
            .sum::<u32>()
    };

    println!("part 1: {}\npart 2: {}", part_fn(false), part_fn(true));
}
