#![allow(unused_imports)]
use itertools::Itertools;

pub fn main() {
    let instructions = shared::puzzle_input!()
        .trim_end()
        .split('\n')
        .map(|line| {
            let (head, rest) = line.split_at(1);
            (head.chars().next().unwrap(), rest.parse::<i32>().unwrap())
        })
        .collect_vec();

    let p1 = instructions
        .iter()
        .fold((0, 0, 1, 0), |(x, y, dx, dy), (what, how_much)| {
            match (what, how_much) {
                ('N', _) => (x, y + how_much, dx, dy),
                ('S', _) => (x, y - how_much, dx, dy),
                ('E', _) => (x + how_much, y, dx, dy),
                ('W', _) => (x - how_much, y, dx, dy),
                ('F', _) => (x + dx * how_much, y + dy * how_much, dx, dy),
                ('L', 90) | ('R', 270) => (x, y, -dy, dx),
                ('L', 270) | ('R', 90) => (x, y, dy, -dx),
                ('L', 180) | ('R', 180) => (x, y, -dx, -dy),
                _ => panic!(),
            }
        });
    let p1 = p1.0.abs() + p1.1.abs();

    let p2 = instructions
        .iter()
        .fold((0, 0, 10, 1), |(x, y, wx, wy), (what, how_much)| {
            match (what, how_much) {
                ('N', _) => (x, y, wx, wy + how_much),
                ('S', _) => (x, y, wx, wy - how_much),
                ('E', _) => (x, y, wx + how_much, wy),
                ('W', _) => (x, y, wx - how_much, wy),
                ('F', _) => (x + wx * how_much, y + wy * how_much, wx, wy),
                ('L', 90) | ('R', 270) => (x, y, -wy, wx),
                ('L', 270) | ('R', 90) => (x, y, wy, -wx),
                ('L', 180) | ('R', 180) => (x, y, -wx, -wy),
                _ => panic!(),
            }
        });
    let p2 = p2.0.abs() + p2.1.abs();

    println!("part 1: {}\npart 2: {}", p1, p2);
}
