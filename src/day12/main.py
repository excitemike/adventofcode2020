def main(puzzle_input):
    # ---- day-specific code start
    instructions = [(line[0], int(line[1:])) for line in puzzle_input.strip().splitlines()]
    directions = {'N': (0, 1), 'S': (0, -1), 'E': (1, 0), 'W': (-1, 0)}

    facingx, facingy = directions['E']
    x, y = 0, 0
    for instruction in instructions:
        [dir_code, distance, *_] = instruction
        move_x, move_y = 0, 0
        if dir_code == 'F':
            move_x, move_y = facingx, facingy
        elif dir_code == 'R':
            for _ in range(0, distance, 90):
                facingx, facingy = facingy, -facingx
        elif dir_code == 'L':
            for _ in range(0, distance, 90):
                facingx, facingy = -facingy, facingx
        else:
            move_x, move_y = directions[dir_code]
        x += move_x * distance
        y += move_y * distance
    p1 = abs(x) + abs(y)

    x, y = 0, 0
    wx, wy = 10, 1
    for instruction in instructions:
        [dir_code, distance, *_] = instruction
        if dir_code == 'F':
            x += distance * wx
            y += distance * wy
        elif dir_code == 'R':
            for _ in range(0, distance, 90):
                wx, wy = wy, -wx
        elif dir_code == 'L':
            for _ in range(0, distance, 90):
                wx, wy = -wy, wx
        else:
            move_x, move_y = directions[dir_code]
            wx += move_x * distance
            wy += move_y * distance

    p2 = abs(x) + abs(y)

    print(f"part 1: {p1}\npart 2: {p2}")
    # ---- day-specific code end

if __name__ == "__main__":
    import os
    import subprocess
    day = int(os.path.split(__file__)[0][-2:])
    main(subprocess.run(f'python src/shared/get_puzzle_input.py {day}'.split(), capture_output=True, text=True).stdout)
