#![allow(unused_imports)]
use itertools::Itertools;
use lazy_static::lazy_static;
use regex::Regex;
use std::collections::HashMap;
lazy_static! {
    static ref REGEX: Regex = Regex::new(r"^\w+\[(\d+)\]$").unwrap();
}

fn read_mask(s: &str) -> (u64, u64) {
    s.chars().fold((0, 0), |(and_bits, or_bits), c| match c {
        '1' => (and_bits << 1 | 1, or_bits << 1 | 1),
        '0' => (and_bits << 1, or_bits << 1),
        _ => (and_bits << 1 | 1, or_bits << 1),
    })
}

fn p1(instructions: &[(&str, &str)]) -> u64 {
    let mut mask = (0, 0);
    let mut memory = HashMap::new();
    for (lhs, rhs) in instructions {
        if *lhs == "mask" {
            mask = read_mask(rhs);
        } else {
            let dst = REGEX
                .captures(lhs)
                .unwrap()
                .get(1)
                .unwrap()
                .as_str()
                .parse::<u64>()
                .unwrap();
            memory.insert(dst, rhs.parse::<u64>().unwrap() & mask.0 | mask.1);
        }
    }
    memory.values().sum()
}

fn p2(instructions: &[(&str, &str)]) -> u64 {
    fn write_masked(memory: &mut HashMap<u64, u64>, mask: &[char], dst: u64, value: u64) {
        let addresses = mask
            .iter()
            .enumerate()
            .fold(vec![dst], |addrs, (bit, c)| match c {
                '0' => addrs,
                '1' => addrs.iter().map(|addr| addr | (1 << bit)).collect_vec(),
                _ => {
                    let mut updated_addrs = Vec::with_capacity(2 * addrs.len());
                    for addr in addrs {
                        updated_addrs.push(addr | (1 << bit));
                        updated_addrs.push(addr & (!(1 << bit)));
                    }
                    updated_addrs
                }
            });
        for addr in addresses {
            memory.insert(addr, value);
        }
    }
    let mut mask = Vec::with_capacity(36);
    let mut memory = HashMap::new();
    for (lhs, rhs) in instructions {
        if *lhs == "mask" {
            mask.clear();
            mask.extend(rhs.chars().rev());
        } else {
            let dst = REGEX
                .captures(lhs)
                .unwrap()
                .get(1)
                .unwrap()
                .as_str()
                .parse::<u64>()
                .unwrap();
            write_masked(&mut memory, &mask, dst, rhs.parse::<u64>().unwrap());
        }
    }
    memory.values().sum()
}

pub fn main() {
    let input = shared::puzzle_input!();
    let instructions = input
        .trim_end()
        .split('\n')
        .map(|line| line.split(" = ").collect_tuple().unwrap())
        .collect_vec();

    println!(
        "part 1: {}\npart 2: {}",
        p1(&instructions),
        p2(&instructions)
    );
}
