def main(puzzle_input):
    # ---- day-specific code start
    import re
    import sys
    write_re = re.compile(r'\w+\[(\d+)\]')
    instructions = [line.split(' = ') for line in puzzle_input.strip().splitlines()]
    memory = {}
    and_mask, or_mask = (0, 0)

    def read_mask(s):
        and_mask = 0x000000000
        or_mask = 0x000000000
        for c in s:
            if c == '1':
                and_mask = (and_mask << 1) | 0x1
                or_mask = (or_mask << 1) | 0x1
            elif c == '0':
                and_mask = (and_mask << 1) | 0x0
                or_mask = (or_mask << 1) | 0x0
            elif c == 'X':
                and_mask = (and_mask << 1) | 0x1
                or_mask = (or_mask << 1) | 0x0
            else:
                raise Exception("unhandled!")
        return (and_mask, or_mask)

    def write_masked(dst, value):
        memory[int(dst)] = (int(value) & and_mask) | or_mask

    # p1
    for (lhs, rhs) in instructions:
        if lhs == 'mask':
            and_mask, or_mask = read_mask(rhs)
        else:
            m = write_re.match(lhs)
            write_masked(m.group(1), rhs)
    p1 = sum(memory.values())

    # p2
    memory = {}
    mask = ['0'] * 36

    def write_masked_v2(dst, value):
        addresses = [int(dst)]
        for bit, c in enumerate(mask):
            if c == '0':
                pass
            elif c == '1':
                addresses = [addr | (1 << bit) for addr in addresses]
            else:
                addresses = [
                    (addr & and_term) | or_term
                    for addr in addresses
                    for (and_term, or_term) in [
                        (0xFFFFFFFFF, 1 << bit),
                        (~(1 << bit), 0)]]
        for addr in addresses:
            memory[addr] = int(value)
    for (lhs, rhs) in instructions:
        if lhs == 'mask':
            mask = list(reversed(rhs))
        else:
            m = write_re.match(lhs)
            write_masked_v2(m.group(1), rhs)
    p2 = sum(memory.values())

    print(f"part 1: {p1}\npart 2: {p2}")
    # ---- day-specific code end


if __name__ == "__main__":
    import os
    import subprocess
    day = int(os.path.split(__file__)[0][-2:])
    main(subprocess.run(f'python src/shared/get_puzzle_input.py {day}'.split(), capture_output=True, text=True).stdout)
