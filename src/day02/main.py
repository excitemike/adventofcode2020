def main(puzzle_input):
    import re
    def translate(s):
        lowest, highest, letter, pwd = re.split('(?:: )|-| ', s)
        return (int(lowest), int(highest), letter, pwd)
    def is_valid_p1(entry):
        (lowest, highest, letter, pwd) = entry
        count = sum(1 for c in pwd if c==letter)
        return lowest <= count <= highest
    entries = list(map(translate, puzzle_input.strip().splitlines()))
    print(f"part 1: {sum(1 for e in entries if is_valid_p1(e))}")
    def is_valid_p2(entry):
        (pos1, pos2, letter, pwd) = entry
        return (pwd[pos1-1] == letter) != (pwd[pos2-1] == letter)
    print(f"part 2: {sum(1 for e in entries if is_valid_p2(e))}")

if __name__ == "__main__":
    import os
    import subprocess
    day = int(os.path.split(__file__)[0][-2:])
    main(subprocess.run(f'python src/shared/get_puzzle_input.py {day}'.split(), capture_output=True, text=True).stdout)