use unicode_segmentation::UnicodeSegmentation;
pub fn main() {
    let puzzle_input = shared::puzzle_input!();
    let data = puzzle_input
        .trim_end()
        .split('\n')
        .map(|line| {
            if let [lowest, highest, letter, pwd, ..] = line
                .unicode_words()
                .filter(|s| !s.trim().is_empty())
                .collect::<Vec<&str>>()
                .as_slice()
            {
                (
                    lowest.parse::<usize>().unwrap(),
                    highest.parse::<usize>().unwrap(),
                    letter.chars().next().unwrap(),
                    pwd.to_string(),
                )
            } else {
                panic!()
            }
        })
        .collect::<Vec<_>>();
    let p1 = data
        .iter()
        .filter(|(lowest, highest, letter, pwd)| {
            let count = pwd.chars().filter(|c| c == letter).count();
            (lowest <= &count) && (&count <= highest)
        })
        .count();
    let p2 = data
        .iter()
        .filter(|(pos1, pos2, letter, pwd)| {
            let v = pwd.chars().collect::<Vec<_>>();
            (&v[pos1 - 1] == letter) != (&v[pos2 - 1] == letter)
        })
        .count();
    println!("part 1: {}\npart 2: {}", p1, p2)
}
