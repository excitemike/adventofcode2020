#![allow(unused_imports)]
use crate::puzzle_input;
use std::cmp::Ordering::*;
use std::cmp::max;
use std::collections::HashMap;

fn coords() -> (Vec<(i32, i32)>, i32, i32, i32, i32) {
    let mut coords = Vec::new();
    let mut left_edge = 341;
    let mut right_edge = 341;
    let mut top_edge = 330;
    let mut bottom_edge = 330;
    for line in puzzle_input!().split('\n') {
        let mut xy = line.trim().split(',');
        let x = xy.next().unwrap().trim().parse::<i32>().unwrap();
        let y = xy.next().unwrap().trim().parse::<i32>().unwrap();
        coords.push((x,y));
        if x < left_edge {
            left_edge = x;
        }
        if x > right_edge {
            right_edge = x;
        }
        if y < top_edge {
            top_edge = y;
        }
        if y > bottom_edge {
            bottom_edge = y;
        }
    }
    
    (
        coords,
        left_edge,
        right_edge,
        bottom_edge,
        top_edge
    )
}

fn nearest(gridx: i32, gridy: i32, coords: &[(i32, i32)]) -> Option<i32> {
    let mut nearest = None;
    let mut best_distance = i32::MAX;
    for (i, (sitex, sitey)) in coords.iter().enumerate() {
        let distance = (sitex - gridx).abs() + (sitey - gridy).abs();
        match distance.cmp(&best_distance) {
            Less => {
                best_distance = distance;
                nearest = Some(i as i32);
            }
            Equal => {
                nearest = None;
            }
            Greater => ()
        }
    }
    nearest
}

pub fn main () -> Result<String, std::io::Error> {
    let (
        coords,
        left_edge,
        right_edge,
        bottom_edge,
        top_edge
    ) = coords();
    
    let mut rows = Vec::new();
    for row in top_edge..=bottom_edge {
        let mut cells = Vec::new();
        for col in left_edge..=right_edge {
            cells.push(nearest(col, row, &coords));
        }
        rows.push(cells);
    }
    
    let mut counts = vec![0; coords.len()];
    for row in rows.iter() {
        for cell in row {
            match cell {
                None => (),
                Some(i) => counts[*i as usize] += 1
            }
        }
    }
    
    // rule out infinites
    // walk left edge
    for row in rows.iter() {
        match row.first().unwrap() {
            None => (),
            Some(i) => counts[*i as usize] = 0
        }
    }
    // walk right edge
    for row in rows.iter() {
        match row.last().unwrap() {
            None => (),
            Some(i) => counts[*i as usize] = 0
        }
    }
    // walk top edge
    for cell in rows.first().unwrap() {
        match cell {
            None => (),
            Some(i) => counts[*i as usize] = 0
        }
    }
    // walk bottom edge
    for cell in rows.last().unwrap() {
        match cell {
            None => (),
            Some(i) => counts[*i as usize] = 0
        }
    }
    
    let biggest = counts.iter().fold(0, |a, b| max(a, *b));
    
    let mut p2 = 0;
    for row in top_edge..=bottom_edge {
        for col in left_edge..=right_edge {
            let mut sum = 0;
            for (x, y) in &coords {
                sum += (col-x).abs() + (row-y).abs();
            }
            if sum < 10000 {
                p2 += 1;
            }
        }
    }
    
    Ok(format!("part 1: {}\npart 2: {}", biggest, p2))
}
