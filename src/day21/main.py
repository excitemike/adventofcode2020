def main(puzzle_input):
    # ---- day-specific code start
    def make_recipe(line: str):
        ingredients, allergens = line.split('(contains ', 1)
        ingredients = ingredients.strip().split()
        allergens = allergens.strip(')').split(', ')
        return (ingredients, allergens)

    recipes = [make_recipe(line) for line in puzzle_input.strip().splitlines()]
    unsolved_ingredients = {i for r in recipes for i in r[0]}
    unsolved_allergens = {i for r in recipes for i in r[1]}
    data = {allergen : unsolved_ingredients for allergen in unsolved_allergens }

    # for each allergen listed, we know only listed ingredients may contain it
    for (ingredients, allergens) in recipes:
        for allergen in allergens:
            data[allergen] = data[allergen] - (unsolved_ingredients - set(ingredients))

    known_safe = {i for i in unsolved_ingredients
                    if all(i not in data[a] for a in unsolved_allergens)}

    p1 = sum(1 for r in recipes
               for i in r[0]
               if i in known_safe)

    while True:
        solutions = []
        # for each allergen, check for all but one ingredient being eliminated
        for allergen in unsolved_allergens:
            if len(data[allergen]) == 1:
                ingredient = next(iter(data[allergen]))
                solutions.append((ingredient, allergen))

        # quit when that stops working
        if not solutions:
            break

        for (ingredient, allergen) in solutions:
            # mark solved
            unsolved_ingredients -= {ingredient}
            unsolved_allergens -= {allergen}
            # remove ingredient from other allergens
            for other_allergen in unsolved_allergens:
                data[other_allergen] -= {ingredient}

    p2 = ','.join(next(iter(ingredients)) for (_, ingredients) in sorted(data.items(), key=lambda x: x[0]))

    print(f"part 1: {p1}\npart 2: {p2}")
    # ---- day-specific code start

if __name__ == "__main__":
    import os
    import subprocess
    day = int(os.path.split(__file__)[0][-2:])
    main(subprocess.run(f'python src/shared/get_puzzle_input.py {day}'.split(), capture_output=True, text=True).stdout)
