#![allow(unused_imports)]
use std::{
    collections::{HashMap, HashSet},
    process::exit,
};

use itertools::Itertools;

pub fn main() {
    let input = shared::puzzle_input!();
    let recipes = input
        .trim_end()
        .split('\n')
        .map(|line| {
            let (ingredients, allergens) = line.split("(contains ").collect_tuple().unwrap();
            let ingredients = ingredients.trim_end().split(' ').collect_vec();
            let allergens = allergens.trim_end_matches(')');
            let allergens = allergens.split(", ").collect_vec();
            (ingredients, allergens)
        })
        .collect_vec();
    let mut unsolved_ingredients: HashSet<&str> = recipes
        .iter()
        .flat_map(|(ingredients, _)| ingredients.iter().cloned())
        .collect();
    let mut unsolved_allergens: HashSet<&str> = recipes
        .iter()
        .flat_map(|(_, allergens)| allergens.iter().cloned())
        .collect();
    let mut data: HashMap<&str, HashSet<&str>> = HashMap::new();
    for &allergen in unsolved_allergens.iter() {
        data.insert(allergen, unsolved_ingredients.iter().cloned().collect());
    }

    // for each allergen listed, we know only listed ingredients may contain it
    for (recipe_ingredients, recipe_allergens) in recipes.iter() {
        for &allergen in recipe_allergens {
            for &ingredient in
                unsolved_ingredients.difference(&recipe_ingredients.iter().cloned().collect())
            {
                data.get_mut(allergen).unwrap().remove(ingredient);
            }
        }
    }

    let known_safe = unsolved_ingredients
        .iter()
        .filter(|&&ingredient| {
            unsolved_allergens
                .iter()
                .all(|allergen| !data.get(allergen).unwrap().contains(ingredient))
        })
        .cloned()
        .collect::<HashSet<_>>();

    let p1: usize = recipes
        .iter()
        .flat_map(|(ingredients, _)| ingredients.iter())
        .filter(|&&ingredient| known_safe.contains(ingredient))
        .count();

    loop {
        let mut solutions = Vec::new();

        // for each allergen, check for all but one ingredient being eliminated
        for &allergen in unsolved_allergens.iter() {
            let set = data.get(allergen).unwrap();
            if 1 == set.len() {
                let &ingredient = set.iter().next().unwrap();
                solutions.push((ingredient, allergen));
            }
        }

        // quit when that stops working
        if solutions.is_empty() {
            break;
        }

        for (ingredient, allergen) in solutions.drain(..) {
            // mark solved
            unsolved_ingredients.remove(ingredient);
            unsolved_allergens.remove(allergen);
            // remove ingredient from other allergens
            for &other_allergen in unsolved_allergens.iter() {
                if allergen != other_allergen {
                    data.get_mut(other_allergen).unwrap().remove(ingredient);
                }
            }
        }
    }

    let p2 = data
        .iter()
        .sorted_by_key(|(&allergen, _)| allergen)
        .map(|(_, ingredients)| ingredients.iter().next().unwrap())
        .join(",");

    println!("part 1: {}\npart 2: {}", p1, p2);
}
