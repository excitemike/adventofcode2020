use itertools::Itertools;

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
enum Cell {
    EmptySeat,
    TakenSeat,
    Floor,
    None,
}

fn adjacent_cells(input: &[Vec<Cell>], row: usize, col: usize) -> Vec<Cell> {
    let mut output = Vec::new();
    if row >= 1 && col >= 1 {
        output.push(get_cell(input, row - 1, col - 1));
    }
    if col >= 1 {
        output.push(get_cell(input, row, col - 1));
    }
    if col >= 1 {
        output.push(get_cell(input, row + 1, col - 1));
    }
    if row >= 1 {
        output.push(get_cell(input, row - 1, col));
    }
    output.push(get_cell(input, row + 1, col));
    if row >= 1 {
        output.push(get_cell(input, row - 1, col + 1));
    }
    output.push(get_cell(input, row, col + 1));
    output.push(get_cell(input, row + 1, col + 1));
    output
}

fn los_cells_helper(
    input: &[Vec<Cell>],
    row: usize,
    col: usize,
    step: &(isize, isize),
) -> Option<Cell> {
    let rows = input.len();
    let cols = input[0].len();
    let mut row = row as isize;
    let mut col = col as isize;
    loop {
        row += step.0;
        col += step.1;
        if row < 0 || col < 0 || row as usize >= rows || col as usize >= cols {
            return None;
        }
        match get_cell(input, row as usize, col as usize) {
            Cell::EmptySeat => return Some(Cell::EmptySeat),
            Cell::TakenSeat => return Some(Cell::TakenSeat),
            Cell::Floor => {}
            Cell::None => {}
        }
    }
}
fn los_cells(input: &[Vec<Cell>], row: usize, col: usize) -> Vec<Cell> {
    let mut output = Vec::new();
    for step in [
        (-1, -1),
        (-1, 0),
        (-1, 1),
        (0, -1),
        (0, 1),
        (1, -1),
        (1, 0),
        (1, 1),
    ]
    .iter()
    {
        if let Some(cell) = los_cells_helper(input, row, col, step) {
            output.push(cell)
        }
    }
    output
}

fn get_cell(input: &[Vec<Cell>], row: usize, col: usize) -> Cell {
    match input.get(row) {
        None => Cell::None,
        Some(row_data) => match row_data.get(col) {
            None => Cell::None,
            Some(cell) => *cell,
        },
    }
}

fn set_cell(input: &mut [Vec<Cell>], row: usize, col: usize, value: Cell) {
    match input.get_mut(row) {
        None => panic!(),
        Some(row_data) => match row_data.get_mut(col) {
            None => panic!(),
            Some(cell) => *cell = value,
        },
    }
}

fn tick(input: &[Vec<Cell>], output: &mut [Vec<Cell>]) {
    let rows = input.len();
    let cols = input[0].len();
    for row in 0..rows {
        for col in 0..cols {
            match get_cell(input, row, col) {
                Cell::EmptySeat => {
                    let neighbors = adjacent_cells(input, row, col);
                    if !neighbors.iter().any(|c| matches!(c, Cell::TakenSeat)) {
                        set_cell(output, row, col, Cell::TakenSeat);
                    }
                }
                Cell::TakenSeat => {
                    let neighbors = adjacent_cells(input, row, col);
                    if 4 <= neighbors
                        .iter()
                        .filter(|&c| matches!(c, Cell::TakenSeat))
                        .count()
                    {
                        set_cell(output, row, col, Cell::EmptySeat);
                    }
                }
                Cell::Floor => {}
                Cell::None => panic!(),
            }
        }
    }
}

fn tick_p2(input: &[Vec<Cell>], output: &mut [Vec<Cell>]) {
    let rows = input.len();
    let cols = input[0].len();
    for row in 0..rows {
        for col in 0..cols {
            match get_cell(input, row, col) {
                Cell::EmptySeat => {
                    let neighbors = los_cells(input, row, col);
                    if !neighbors.iter().any(|c| matches!(c, Cell::TakenSeat)) {
                        set_cell(output, row, col, Cell::TakenSeat);
                    }
                }
                Cell::TakenSeat => {
                    let neighbors = los_cells(input, row, col);
                    if 5 <= neighbors
                        .iter()
                        .filter(|&c| matches!(c, Cell::TakenSeat))
                        .count()
                    {
                        set_cell(output, row, col, Cell::EmptySeat);
                    }
                }
                Cell::Floor => {}
                Cell::None => panic!(),
            }
        }
    }
}

#[allow(dead_code)]
fn print_state(data: &[Vec<Cell>]) {
    let empty = console::Style::new().green().bold();
    let taken = console::Style::new().white().bold();
    let floor = console::Style::new().red().bold();
    for row in data {
        for cell in row {
            match cell {
                Cell::EmptySeat => print!("{}", empty.apply_to("L ")),
                Cell::TakenSeat => print!("{}", taken.apply_to("# ")),
                Cell::Floor => print!("{}", floor.apply_to(". ")),
                Cell::None => print!("⚠ "),
            }
        }
        println!(". ");
    }
}

fn p1(data: &[Vec<Cell>]) -> usize {
    let mut state_a = data.to_vec();
    let mut state_b = data.to_vec();
    loop {
        tick(&state_a, &mut state_b);
        if state_a == state_b {
            break;
        }
        state_a.clone_from(&state_b);
    }

    if console::user_attended() {
        print_state(&state_a);
        std::thread::sleep(std::time::Duration::new(0, 125_000_000))
    }

    state_a
        .iter()
        .flat_map(|row_data| row_data.iter().filter(|&&cell| cell == Cell::TakenSeat))
        .count()
}

fn p2(data: &[Vec<Cell>]) -> usize {
    let mut state_a = data.to_vec();
    let mut state_b = data.to_vec();
    loop {
        tick_p2(&state_a, &mut state_b);
        if state_a == state_b {
            break;
        }
        state_a.clone_from(&state_b);
    }
    state_a
        .iter()
        .flat_map(|row_data| row_data.iter().filter(|&&cell| cell == Cell::TakenSeat))
        .count()
}

pub fn main() {
    let puzzle_input = shared::puzzle_input!();
    let original = puzzle_input
        .trim_end()
        .split('\n')
        .map(|line| {
            line.chars()
                .map(|c| match c {
                    'L' => Cell::EmptySeat,
                    '.' => Cell::Floor,
                    _ => panic!(),
                })
                .collect_vec()
        })
        .collect_vec();

    println!("part 1: {}\npart 2: {}", p1(&original), p2(&original));
}
