def main(puzzle_input):
    # ---- day-specific code start
    offsets = [(-1,-1),(-1,0),(-1,1),(0,-1),(0,1),(1,-1),(1,0),(1,1)]

    def neighbors(data, row, col):
        return [data[row+orow][col+ocol] for (orow,ocol) in offsets
                               if (0 <= row+orow < len(data))
                               and 0 <= col+ocol < len(data[row+orow])]
    
    def los_cells_helper(data, row, col, step):
        row_step, col_step = step
        rows = len(data)
        cols = len(data[0])
        while True:
            row += row_step
            col += col_step
            if (0<=row<rows) and (0<=col<cols):
                cell = data[row][col]
                if (cell == 'L') or (cell == '#'):
                    return cell
            else:
                return None
        return None

    def los_cells(data, row, col):
        return [cell for step in offsets
                     if (cell := los_cells_helper(data,
                                                  row,
                                                  col,
                                                  step)) is not None]

    def tick(data, rules_fn):
        return [delta for (row_num, row_data) in enumerate(data)
                      for (col_num, cell) in enumerate(row_data)
                      if (delta := rules_fn(data,
                                            row_num,
                                            col_num)) is not None]

    def rules_p1(data, row_num, col_num):
        cell = data[row_num][col_num]
        ns = neighbors(data, row_num, col_num)
        if cell == 'L':
            if all(cell != '#' for cell in ns):
                return (row_num, col_num, '#')
        elif cell == '#':
            if 4 <= sum(1 for cell in ns
                          if cell == '#'):
                return (row_num, col_num, 'L')
        return None

    def rules_p2(data, row_num, col_num):
        cell = data[row_num][col_num]
        ns = los_cells(data, row_num, col_num)
        if cell == 'L':
            if all(cell != '#' for cell in ns):
                return (row_num, col_num, '#')
        elif cell == '#':
            if 5 <= sum(1 for cell in ns
                          if cell == '#'):
                return (row_num, col_num, 'L')
        return None

    def run_til_stable_count_taken_seats(data, rules_fn):
        buf = [row.copy() for row in data]
        gen = 0
        while True:
            gen += 1
            deltas = tick(buf, rules_fn)
            if not deltas:
                break
            for (row, col, cell) in deltas:
                buf[row][col] = cell
        return sum(1 for row in buf for cell in row if cell == '#')

    p1 = lambda data: run_til_stable_count_taken_seats(data, rules_p1)
    p2 = lambda data: run_til_stable_count_taken_seats(data, rules_p2)

    data = [[c for c in line.strip()] for line in puzzle_input.strip().splitlines()]
    print(f"part 1: {p1(data)}\npart 2: {p2(data)}")
    # ---- day-specific code end

if __name__ == "__main__":
    import os
    import subprocess
    day = int(os.path.split(__file__)[0][-2:])
    main(subprocess.run(f'python src/shared/get_puzzle_input.py {day}'.split(), capture_output=True, text=True).stdout)

