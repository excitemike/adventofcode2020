#![allow(unused_imports)]
use itertools::Itertools;

fn multiplicative_inverse_modulo(a: isize, m: isize) -> isize {
    for x in 0..m {
        if a * x % m == 1 {
            return x;
        }
    }
    panic!()
}

fn p2(earliest: isize, busses: &[(isize, isize)]) -> isize {
    let mut t = earliest;
    let mut step = 1;
    for (offset, period) in busses {
        let mul_inv = multiplicative_inverse_modulo(step, *period);
        let times = ((-t - offset) * mul_inv) % period;
        let times = (period + times) % period;
        t += step * times;
        step *= period;
    }
    t
}

pub fn main() {
    let input = shared::puzzle_input!();
    let mut lines = input.trim_end().split('\n');
    let earliest = lines.next().unwrap().parse::<isize>().unwrap();
    let busses = lines
        .next()
        .unwrap()
        .split(',')
        .enumerate()
        .filter(|(_, bus)| bus != &"x")
        .map(|(offset, bus)| (offset as isize, bus.parse::<isize>().unwrap()))
        .collect_vec();
    let earliest_arrival = busses
        .iter()
        .map(|(_, route_time)| {
            (
                route_time,
                (earliest + route_time - 1) / route_time * route_time,
            )
        })
        .min_by_key(|x| x.1)
        .unwrap();
    let p1 = earliest_arrival.0 * (earliest_arrival.1 - earliest);

    println!("part 1: {}\npart 2: {}", p1, p2(earliest, &busses[..]));
}
