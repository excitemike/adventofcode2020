fn multiplicative_inverse_modulo a m =
    for x in 0..m,
        if a * x % m == 1,
            return x
    panic
}

fn p2 earliest busses =
    let t = earliest
    let step = 1
    for (offset, period) in busses,
        let mul_inv = multiplicative_inverse_modulo(step, *period)
        let times = ((-t - offset) * mul_inv) mod period
        let times = (period + times) mod period
        t += step * times
        step *= period
    t
}

let lines = read_to_string "puzzle_input/day13"
    . trim_end
    . split '\n'
    . to_vec
let earliest = lines[0] . parse_int . unwrap
let busses = [(offset, parse_int bus . unwrap) for bus in lines[1] . split ',', if bus != "x"]
let p1 = do
    fn earliest_arrival_fn route_time = (earliest + route_time - 1) / route_time * route_time
    let bus_id, time = [(route_time, earliest_arrival_fn route_time) for (_, route_time) in busses]
        . min_by_key \ (_, arrival) = arrival
        . unwrap
    bus_id * (time - earliest)

f"part 1: {p1}\npart 2: {p2 earliest busses}"
