def p2_naive(earliest, contest_reqs):
    contest_reqs.sort(key=lambda x: -x[1])
    t = earliest

    busses_satisfied = 0
    step = 1
    while True:
        if busses_satisfied == len(contest_reqs):
            break
        else:
            offset, bus = contest_reqs[busses_satisfied]
            if (t+offset) % bus == 0:
                busses_satisfied += 1
                step *= bus
                #print(f'satisfied bus {bus}. now stepping by {step}')
            else:
                t += step
    return t

def p2_smart_way(earliest, contest_reqs):
    def multiplicative_inverse_modulo(a, m):
        for x in range(0, m):
            if a*x % m == 1:
                return x
        raise Exception()

    t = earliest
    step = 1
    for offset, bus in contest_reqs:
        mul_inv = multiplicative_inverse_modulo(step, bus)
        # condition for this bus is satisfied when (t'+offset) % bus == 0
        # treating t' as a number of steps from t: (times*step+t+offset) % bus == 0
        # written as modulo arithmetic: times*step+t+offset <congruent to> 0 (mod bus)
        # rearranged: times*step <congruent to> -t-offset (mod bus)
        # multiplying each side by the multiplicative inverse of step modulo bus (mul_inv): times <congruent to> (-t-offset) * mul_inv (mod bus)
        times = ((-t-offset) * mul_inv) % bus
        t += step * times
        #print(f'multiplicative inverse of {step} modulo {bus} is {multiplicative_inverse_modulo(step, bus)}.')
        #print(f'take the min step {times} times (step {step*times} to reach {t})')
        #print()
        step *= bus
        #print(f'{t} satisfies bus {bus}.')
        #print(f'step sizes that are a multiple of that will keep this bus\'s condition satisfied.')
        #print(f'so steps must now be a multiple of {step}')
        #print()
    return t

def main(puzzle_input):
    # ---- day-specific code start
    [earliest, rest] = puzzle_input.strip().splitlines()
    earliest = int(earliest)
    busses = [x for x in rest.split(',')]

    # p1
    busses_in_service = [int(bus) for bus in busses if bus != 'x']
    bus_arrivals = [(bus, ((earliest + bus - 1) // bus * bus)) for bus in busses_in_service]
    p1_id, p1_time = min(*bus_arrivals, key=lambda x: x[1])
    p1 = p1_id * (p1_time - earliest)

    #p2
    contest_reqs = [(i, int(bus)) for (i, bus) in enumerate(busses) if bus != 'x']
    t = p2_smart_way(earliest, contest_reqs)
    assert t == p2_naive(earliest, contest_reqs)

    print(f"part 1: {p1}\npart 2: {t}")
    # ---- day-specific code end

if __name__ == "__main__":
    import os
    import subprocess
    day = int(os.path.split(__file__)[0][-2:])
    main(subprocess.run(f'python src/shared/get_puzzle_input.py {day}'.split(), capture_output=True, text=True).stdout)
