use itertools::Itertools;
use std::collections::{HashMap, HashSet};

struct Data<'a> {
    field_info: HashMap<&'a str, (usize, usize, usize, usize)>,
    your_ticket: Vec<usize>,
    nearby_tickets: Vec<Vec<usize>>,
}

fn get_data(input: &'_ str) -> Data<'_> {
    let field_info_end = input.find("\n\n").unwrap();
    let field_info = (input[..field_info_end])
        .split('\n')
        .map(|line| {
            let (field_name, rhs) = line.split(": ").collect_tuple().unwrap();
            (
                field_name,
                rhs.split(" or ")
                    .flat_map(|pair| pair.split('-'))
                    .map(|x| x.parse::<usize>().unwrap())
                    .collect_tuple()
                    .unwrap(),
            )
        })
        .collect::<HashMap<_, _>>();

    let ticket_hdr = "your ticket:\n";
    let your_ticket_start = input.find(ticket_hdr).unwrap() + ticket_hdr.len();
    let your_ticket_end = your_ticket_start + input[your_ticket_start..].find('\n').unwrap();
    let your_ticket = (input[your_ticket_start..your_ticket_end])
        .split(',')
        .map(|s| s.parse::<usize>().unwrap())
        .collect_vec();

    let nearby_hdr = "nearby tickets:\n";
    let nearby_tickets_start = input.find(nearby_hdr).unwrap() + nearby_hdr.len();
    let nearby_tickets = (input[nearby_tickets_start..])
        .split('\n')
        .map(|line| {
            line.split(',')
                .map(|s| s.parse::<usize>().unwrap())
                .collect_vec()
        })
        .collect_vec();

    Data {
        field_info,
        your_ticket,
        nearby_tickets,
    }
}

pub fn main() {
    let input = shared::puzzle_input!().trim_end().replace("\r\n", "\n");
    let data = get_data(&input);
    fn valid_for_some_field(
        x: &usize,
        field_info: &HashMap<&str, (usize, usize, usize, usize)>,
    ) -> bool {
        field_info
            .values()
            .any(|&(a, b, c, d)| (a..=b).contains(x) || (c..=d).contains(x))
    }
    let p1: usize = data
        .nearby_tickets
        .iter()
        .flatten()
        .filter(|x| !valid_for_some_field(x, &data.field_info))
        .sum();
    let valid_tickets = data
        .nearby_tickets
        .iter()
        .filter(|&ticket| {
            ticket
                .iter()
                .all(|x| valid_for_some_field(x, &data.field_info))
        })
        .collect_vec();
    let mut possibilities: Vec<HashSet<&str>> = (0..data.your_ticket.len())
        .map(|i| {
            data.field_info
                .iter()
                .filter(|(_, &(a, b, c, d))| {
                    valid_tickets
                        .iter()
                        .all(|t| (a..=b).contains(&t[i]) || (c..=d).contains(&t[i]))
                })
                .map(|(&s, _)| s)
                .collect()
        })
        .collect();
    // narrow down possibilities
    let mut solved = possibilities
        .iter()
        .filter(|set| set.len() == 1)
        .fold(HashSet::new(), |acc, set| acc.union(set).copied().collect());
    while possibilities.iter().any(|set| set.len() > 1) {
        let unfinished_sets = possibilities.iter_mut().filter(|set| set.len() > 1);
        for set in unfinished_sets {
            let items_to_remove = set.intersection(&solved).copied().collect_vec();
            for x in items_to_remove {
                set.remove(x);
            }
            if set.len() == 1 {
                let new_solved = set.iter().next().unwrap();
                solved.insert(new_solved);
            }
        }
    }
    let fields_in_order = possibilities
        .iter()
        .map(|set| *set.iter().next().unwrap())
        .collect_vec();
    let p2: usize = fields_in_order
        .iter()
        .zip(data.your_ticket.iter())
        .filter(|(&s, _)| s.starts_with("departure"))
        .map(|(_, x)| x)
        .product();
    println!("part 1: {}\npart 2: {}", p1, p2);
}
