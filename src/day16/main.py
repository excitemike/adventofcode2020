def main(puzzle_input):
    # ---- day-specific code start
    from functools import reduce
    from math import prod

    puzzle_input = puzzle_input.strip()
    field_info_end = puzzle_input.find('\n\n')
    field_info_lines = puzzle_input[:field_info_end].splitlines()
    field_info = {k: [int(x)
                      for pair in rhs.split(' or ')
                      for x in pair.split('-')]
                  for [k, rhs] in [line.split(': ')
                                   for line in field_info_lines]}

    ticket_hdr = 'your ticket:\n'
    i = puzzle_input.find(ticket_hdr)
    i += len(ticket_hdr)
    j = puzzle_input.find('\n', i)
    your_ticket = [int(x) for x in puzzle_input[i:j].split(',')]

    nearby_hdr = 'nearby tickets:\n'
    i = puzzle_input.find(nearby_hdr)
    i += len(nearby_hdr)
    nearby_tix = [[int(x) for x in line.split(',')]
                  for line in puzzle_input[i:].splitlines()]

    minimum_for_any = min(*[min(min1, min2)
                            for [min1, _, min2, _] in field_info.values()])
    maximum_for_any = max(*[max(max1, max2)
                            for [_, max1, _, max2] in field_info.values()])

    def invalid_values(tickets):
        for ticket in tickets:
            for x in ticket:
                if (x < minimum_for_any) or (x > maximum_for_any):
                    #print(f'{x} outside of range [{minimum_for_any}, {maximum_for_any}]')
                    yield x
                else:
                    for [a, b, c, d] in field_info.values():
                        if (a <= x <= b) or (c <= x <= d):
                            break
                    else:
                        #print(f'{x} didn\'t match any')
                        yield x

    def ticket_valid(ticket):
        for x in ticket:
            if (x < minimum_for_any) or (x > maximum_for_any):
                return False
            else:
                for [a, b, c, d] in field_info.values():
                    if (a <= x <= b) or (c <= x <= d):
                        break
                else:
                    return False
        return True

    p1 = sum(invalid_values(nearby_tix))

    valid_tickets = [t for t in nearby_tix if ticket_valid(t)]
    possibilities_by_index = [{k for k, [a, b, c, d] in field_info.items()
                               if all((a <= t[i] <= b) or (c <= t[i] <= d) for t in valid_tickets)} for i in range(len(your_ticket))]

    def fst(s):
        for x in s: break
        return x

    while not all(len(p) == 1 for p in possibilities_by_index):
        solved = {fst(s) for s in possibilities_by_index if len(s) == 1}
        for i, possibilities in enumerate(possibilities_by_index):
            if len(possibilities) > 1:
                possibilities_by_index[i] = possibilities_by_index[i] - solved

    p2 = prod([your_value
               for (s, your_value) in zip(possibilities_by_index, your_ticket)
               if list(s)[0].startswith('departure')])

    print(f"part 1: {p1}\npart 2: {p2}")
    # ---- day-specific code end

if __name__ == "__main__":
    import os
    import subprocess
    day = int(os.path.split(__file__)[0][-2:])
    main(subprocess.run(f'python src/shared/get_puzzle_input.py {day}'.split(), capture_output=True, text=True).stdout)
