def main(puzzle_input):
    # ---- day-specific code start
    def input_to_dict(input_string):
        lines = [s.split() for s in input_string.strip().split('\n')]
        contain_map = {}
        for words in lines:
            outer_bag = f'{words[0]} {words[1]}'
            for count, bag1, bag2, _ in four_at_a_time(words[4:]):
                inner_bag = f'{bag1} {bag2}'
                if outer_bag in contain_map:
                    assert inner_bag not in contain_map[outer_bag]
                    contain_map[outer_bag][inner_bag] = int(count)
                else:
                    contain_map[outer_bag] = {inner_bag:int(count)}
        return contain_map

    def four_at_a_time(seq):
        return zip(*[iter(seq)]*4)

    def p1(contain_map):
        possible_containers = set()
        unvisited = {key for key in contain_map.keys()}
        stack = ["shiny gold"]
        unvisited.remove("shiny gold")
        while stack:
            cur = stack.pop()
            for candidate_container, inner_map in contain_map.items():
                if cur in inner_map:
                    possible_containers.add(candidate_container)
                    if candidate_container in unvisited:
                        unvisited.remove(candidate_container)
                        stack.append(candidate_container)
        return len(possible_containers)

    def p2(contain_map):
        stack = ["shiny gold"]
        bags_inside = 0
        while stack:
            cur = stack.pop()
            if cur in contain_map:
                reqs = contain_map[cur]
                for inner_bag, count in reqs.items():
                    for _ in range(0,count):
                        bags_inside += 1
                        stack.append(inner_bag)
        return bags_inside

    contain_map = input_to_dict(puzzle_input)
    print(f"part 1: {p1(contain_map)}\npart 2: {p2(contain_map)}")
    # ---- day-specific code end

if __name__ == "__main__":
    import os
    import subprocess
    day = int(os.path.split(__file__)[0][-2:])
    main(subprocess.run(f'python src/shared/get_puzzle_input.py {day}'.split(), capture_output=True, text=True).stdout)
