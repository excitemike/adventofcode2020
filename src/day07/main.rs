use std::collections::{HashMap, HashSet};
use unicode_segmentation::UnicodeSegmentation;
pub fn main() {
    let puzzle_input = shared::puzzle_input!();
    let mut str_to_id_backing: HashMap<String, usize> = HashMap::new();
    let mut str_to_id = |s: String| -> usize {
        match str_to_id_backing.get(&s) {
            Some(x) => *x,
            None => {
                let id: usize = str_to_id_backing.len();
                str_to_id_backing.insert(s, str_to_id_backing.len());
                id
            }
        }
    };
    let mut contain_map: HashMap<usize, HashMap<usize, usize>> = HashMap::new();
    for line in puzzle_input.trim_end().split('\n') {
        let words: Vec<_> = line
            .unicode_words()
            .filter(|s| !s.trim().is_empty())
            .collect();
        let outer_bag = str_to_id(format!("{} {}", words[0], words[1]));
        let mut i = 4usize;
        while i + 2 < words.len() {
            match words[i] {
                "no" => break,
                n => {
                    let n = n.parse::<usize>().unwrap();
                    let bag = str_to_id(format!("{} {}", words[i + 1], words[i + 2]));
                    i += 4;
                    contain_map
                        .entry(outer_bag)
                        .and_modify(|inner_bags| {
                            inner_bags.insert(bag, n);
                        })
                        .or_insert_with(|| {
                            let mut m = HashMap::new();
                            m.insert(bag, n);
                            m
                        });
                }
            }
        }
    }
    let shiny_gold = str_to_id(String::from("shiny gold"));
    let mut possible_containers: HashSet<usize> = HashSet::new();
    {
        let mut unvisited: HashSet<_> = contain_map.keys().collect();
        unvisited.remove(&shiny_gold);
        let mut stack = vec![shiny_gold];
        while !stack.is_empty() {
            let cur = stack.pop().unwrap();
            for (candidate_container, inner_map) in contain_map.iter() {
                if inner_map.contains_key(&cur) {
                    possible_containers.insert(*candidate_container);
                    if unvisited.contains(candidate_container) {
                        unvisited.remove(&candidate_container);
                        stack.push(*candidate_container)
                    }
                }
            }
        }
        possible_containers.remove(&shiny_gold);
    }

    let mut stack = vec![shiny_gold];
    let mut bags_inside = 0usize;
    while !stack.is_empty() {
        let cur = stack.pop().unwrap();
        if let Some(reqs) = contain_map.get(&cur) {
            for (inner_bag, count) in reqs {
                for _ in 0..*count {
                    bags_inside += 1;
                    stack.push(*inner_bag);
                }
            }
        }
    }

    println!(
        "part 1: {}\npart 2: {}",
        possible_containers.len(),
        bags_inside
    );
}
