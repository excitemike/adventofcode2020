fn do_p1(v: &[u64]) -> u64 {
    for (i, a) in v.iter().enumerate() {
        for b in v[i..].iter() {
            if a + b == 2020 {
                return a * b;
            }
        }
    }
    unreachable!()
}
fn do_p2(v: &[u64]) -> u64 {
    for (i, a) in v.iter().enumerate() {
        for (j, b) in v[i..].iter().enumerate() {
            for c in v[(i + j)..].iter() {
                if a + b + c == 2020 {
                    return a * b * c;
                }
            }
        }
    }
    unreachable!()
}

pub fn main() {
    let puzzle_input = shared::puzzle_input!();
    let v: Vec<u64> = puzzle_input
        .trim_end()
        .split('\n')
        .map(|x| {
            x.trim()
                .parse::<u64>()
                .unwrap_or_else(|_| panic!("parse error!? ({})", x))
        })
        .collect();

    println!("part 1: {}\npart 2: {}", do_p1(&v), do_p2(&v))
}
