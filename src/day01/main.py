def main(puzzle_input):
    entries = list(map(int, puzzle_input.strip().splitlines()))
    print(next(f"part 1: {a*b}"
            for (i, a) in enumerate(entries)
            for b in entries[(i+1):]
            if a+b==2020))
    print(next(f"part 2: {a*b*c}"
            for (i, a) in enumerate(entries)
            for (j, b) in enumerate(entries[(i+1):])
            for c in entries[(i+j+1):]
            if a+b+c==2020))

if __name__ == "__main__":
    import os
    import subprocess
    day = int(os.path.split(__file__)[0][-2:])
    main(subprocess.run(f'python src/shared/get_puzzle_input.py {day}'.split(), capture_output=True, text=True).stdout)