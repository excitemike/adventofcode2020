def main(puzzle_input):
    # ---- day-specific code start
    grid = [[c for c in line] for line in puzzle_input.strip().splitlines()]
    p1 = 0
    p2 = 0

    print(f"part 1: {p1}\npart 2: {p2}")
    # ---- day-specific code start

if __name__ == "__main__":
    import os
    import subprocess
    day = int(os.path.split(__file__)[0][-2:])
    main(subprocess.run(f'python src/shared/get_puzzle_input.py {day}'.split(), capture_output=True, text=True).stdout)
