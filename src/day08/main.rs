#[derive(Clone, Copy, Debug)]
enum Op {
    Acc,
    Jmp,
    Nop,
}

impl From<&str> for Op {
    fn from(s: &str) -> Self {
        match s {
            "acc" => Op::Acc,
            "jmp" => Op::Jmp,
            "nop" => Op::Nop,
            _ => panic!("how'd I get instruction {}", s),
        }
    }
}

fn exec(operation: Op, argument: isize, accumulator: isize, pc: isize) -> (isize, isize) {
    match operation {
        Op::Acc => (accumulator + argument, pc + 1),
        Op::Jmp => (accumulator, pc + argument),
        Op::Nop => (accumulator, pc + 1),
    }
}

fn run_til_loop(instructions: &[(Op, isize)]) -> (isize, isize) {
    let mut accumulator = 0;
    let mut pc = 0isize;
    let mut is_executed = vec![false; instructions.len()];
    loop {
        match instructions.get(pc as usize) {
            Some((operation, argument)) if !is_executed[pc as usize] => {
                is_executed[pc as usize] = true;
                let result = exec(*operation, *argument, accumulator, pc);
                accumulator = result.0;
                pc = result.1;
            }
            _ => break (accumulator, pc),
        }
    }
}

fn p2(instructions: &[(Op, isize)]) -> isize {
    for (i, (op, arg)) in instructions.iter().enumerate() {
        let mut test_instructions = instructions.to_vec();
        match op {
            Op::Jmp => test_instructions[i] = (Op::Nop, *arg),
            Op::Nop => test_instructions[i] = (Op::Jmp, *arg),
            _ => continue,
        };
        let result = run_til_loop(test_instructions.iter().as_slice());
        if result.1 == instructions.len() as isize {
            return result.0;
        }
    }
    0
}

pub fn main() {
    let puzzle_input = shared::puzzle_input!();
    let instructions = puzzle_input
        .trim_end()
        .split('\n')
        .map(|line| {
            let mut x = line.split(' ');
            (x.next().unwrap().into(), x.next().unwrap().parse().unwrap())
        })
        .collect::<Vec<_>>();
    println!(
        "part 1: {}\npart 2: {}",
        run_til_loop(&instructions).0,
        p2(&instructions)
    );
}
