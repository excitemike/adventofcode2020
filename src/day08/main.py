def main(puzzle_input):
    # ---- day-specific code start
    def run_to_loop(instructions):
        accumulator = 0
        pc = 0
        is_executed = [False for _ in instructions]
        while True:
            if pc>=len(is_executed):
                break
            if pc<0:
                break
            if is_executed[pc]:
                break
            else:
                is_executed[pc] = True
                accumulator, pc = exec(instructions[pc], accumulator, pc)
        return (accumulator, pc)

    def exec(instruction, accumulator, pc):
        (operation, argument) = instruction
        if operation == 'acc':
            accumulator += argument
            pc += 1
        elif operation == 'jmp':
            pc += argument
        elif operation == 'nop':
            pass
            pc += 1
        else:
            raise ":("
        return (accumulator, pc)

    def p2(instructions):
        for i in range(0, len(instructions)):
            (operation, argument) = instructions[i]
            if operation == 'jmp':
                test_instructions = list(instructions)
                (_, argument) = instructions[i]
                test_instructions[i] = ('nop', argument)
                accumulator, pc = run_to_loop(test_instructions)
                if pc == len(instructions):
                    return accumulator
            elif operation == 'nop':
                test_instructions = list(instructions)
                (_, argument) = instructions[i]
                test_instructions[i] = ('jmp', argument)
                accumulator, pc = run_to_loop(test_instructions)
                if pc == len(instructions):
                    return accumulator

    instructions = [(operation, int(argument)) for (operation, argument) in (line.split() for line in puzzle_input.strip().splitlines())]
    print(f"part 1: {run_to_loop(instructions)[0]}\npart 2: {p2(instructions)}")
    # ---- day-specific code end

if __name__ == "__main__":
    import os
    import subprocess
    day = int(os.path.split(__file__)[0][-2:])
    main(subprocess.run(f'python src/shared/get_puzzle_input.py {day}'.split(), capture_output=True, text=True).stdout)

