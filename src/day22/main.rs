#![allow(unused_imports)]
use std::collections::{HashSet, VecDeque};

use itertools::Itertools;

pub fn main() {
    let input = shared::puzzle_input!();
    let initial_state = input
        .trim_end()
        .split("\n\n")
        .map(|player_block| {
            player_block
                .split('\n')
                .skip(1)
                .map(|card| card.parse::<u8>().unwrap())
                .collect::<VecDeque<_>>()
        })
        .collect_vec();
    let mut players = initial_state.clone();

    while !players.iter().any(|deck| deck.is_empty()) {
        let top_cards = players
            .iter_mut()
            .map(|deck| deck.pop_front().unwrap())
            .collect_vec();
        match &top_cards[..] {
            [a, b] => match a.cmp(b) {
                std::cmp::Ordering::Less => {
                    players[1].push_back(*b);
                    players[1].push_back(*a);
                }
                std::cmp::Ordering::Equal => panic!(),
                std::cmp::Ordering::Greater => {
                    players[0].push_back(*a);
                    players[0].push_back(*b);
                }
            },
            _ => panic!(),
        }
    }

    let deck_size = players.iter().map(|deck| deck.len()).max().unwrap();
    let p1 = players
        .iter()
        .map(|deck| {
            deck.iter().enumerate().fold(0, |acc, (i, card)| {
                let multiplier = deck_size - i;
                acc + multiplier * (*card as usize)
            })
        })
        .max()
        .unwrap();

    // Part 2
    fn play_game_p2(initial_state: Vec<VecDeque<u8>>) -> (u8, usize) {
        let mut players = initial_state;
        let mut states_seen: HashSet<Vec<VecDeque<u8>>> = HashSet::new();
        while !players.iter().any(|deck| deck.is_empty()) {
            let cur_state = players.clone();
            if states_seen.contains(&cur_state) {
                // player 1 win
                let deck_size = players[0].len();
                let winner = 0;
                let score = players[0].iter().enumerate().fold(0, |acc, (i, card)| {
                    let multiplier = deck_size - i;
                    acc + multiplier * (*card as usize)
                });
                return (winner, score);
            } else {
                states_seen.insert(cur_state);
            }

            // draw top cards
            let top_cards = players
                .iter_mut()
                .map(|deck| deck.pop_front().unwrap())
                .collect_vec();

            // If both players have at least as many cards remaining in
            // their deck as the value of the card they just drew,
            // the winner of the round is determined by playing a new game
            // of Recursive Combat
            let recurse = top_cards
                .iter()
                .zip(players.iter())
                .all(|(drawn_card, deck)| deck.len() >= *drawn_card as usize);
            if recurse {
                let state = top_cards
                    .iter()
                    .zip(players.iter())
                    .map(|(drawn_card, deck)| {
                        deck.iter()
                            .take(*drawn_card as usize)
                            .cloned()
                            .collect::<VecDeque<_>>()
                    })
                    .collect_vec();
                let (winner, _) = play_game_p2(state);
                match (winner, &top_cards[..]) {
                    (0, [a, b]) => {
                        players[0].push_back(*a);
                        players[0].push_back(*b);
                    }
                    (1, [a, b]) => {
                        players[1].push_back(*b);
                        players[1].push_back(*a);
                    }
                    _ => panic!(),
                }
                continue;
            }

            match &top_cards[..] {
                [a, b] => match a.cmp(b) {
                    std::cmp::Ordering::Less => {
                        players[1].push_back(*b);
                        players[1].push_back(*a);
                    }
                    std::cmp::Ordering::Equal => panic!(),
                    std::cmp::Ordering::Greater => {
                        players[0].push_back(*a);
                        players[0].push_back(*b);
                    }
                },
                _ => panic!(),
            }
        }
        let winner = u8::from(players[0].is_empty());
        let deck_size = players[winner as usize].len();
        let score = players[winner as usize]
            .iter()
            .enumerate()
            .fold(0, |acc, (i, card)| {
                let multiplier = deck_size - i;
                acc + multiplier * (*card as usize)
            });
        (winner, score)
    }

    let (_, p2) = play_game_p2(initial_state);

    println!("part 1: {}\npart 2: {}", p1, p2);
}
