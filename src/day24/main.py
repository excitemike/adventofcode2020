from collections import Counter

# ---- day-specific code start

def line_to_coord(line):
    x = 0
    y = 0
    for c in line:
        if c=='n':
            y += 1
        elif c=='s':
            y -= 1
        elif c=='e':
            x = x+2 if ((x+y)%2==0) else x+1
        elif c=='w':
            x = x-2 if ((x+y)%2==0) else x-1
    return (x,y)

def neighbor_addrs(state, addr):
    x, y = addr
    return [(x+2,y),(x+1,y+1),(x-1,y+1),(x-2,y),(x-1,y-1),(x+1,y-1)]

def step_ca(state):
    new_state = set()
    counts = Counter(naddr for addr in state for naddr in neighbor_addrs(state, addr))
    for addr, count in counts.items():
        if 2==count:
            new_state.add(addr)
        elif (addr in state) and (1==count):
            new_state.add(addr)
    return new_state

EXAMPLE = """sesenwnenenewseeswwswswwnenewsewsw
neeenesenwnwwswnenewnwwsewnenwseswesw
seswneswswsenwwnwse
nwnwneseeswswnenewneswwnewseswneseene
swweswneswnenwsewnwneneseenw
eesenwseswswnenwswnwnwsewwnwsene
sewnenenenesenwsewnenwwwse
wenwwweseeeweswwwnwwe
wsweesenenewnwwnwsenewsenwwsesesenwne
neeswseenwwswnwswswnw
nenwswwsewswnenenewsenwsenwnesesenew
enewnwewneswsewnwswenweswnenwsenwsw
sweneswneswneneenwnewenewwneswswnese
swwesenesewenwneswnwwneseswwne
enesenwswwswneneswsenwnewswseenwsese
wnwnesenesenenwwnenwsewesewsesesew
nenewswnwewswnenesenwnesewesw
eneswnwswnwsenenwnwnwwseeswneewsenese
neswnwewnwnwseenwseesewsenwsweewe
wseweeenwnesenwwwswnew"""

def day24b(counts):
    state = set(addr for addr, count in counts.items() if count%2==1)
    for i in range(0,100):
        state = step_ca(state)
    return len(state)

def main(puzzle_input):
    counts = Counter(line_to_coord(line) for line in puzzle_input.strip().splitlines())
    p1 = sum(1 for x in counts.values() if x%2==1)
    p2 = day24b(counts)
    print(f"part 1: {p1}\npart 2: {p2}")
# ---- day-specific code end

if __name__ == "__main__":
    import os
    import subprocess
    day = int(os.path.split(__file__)[0][-2:])
    main(subprocess.run(f'python src/shared/get_puzzle_input.py {day}'.split(), capture_output=True, text=True).stdout)
