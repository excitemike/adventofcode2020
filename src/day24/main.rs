#![allow(unused_imports)]
use itertools::Itertools;
use num_traits::{One, Zero};
use std::{
    cmp::{max, min},
    collections::{HashMap, HashSet},
    iter::FromIterator,
    ops::AddAssign,
};

trait Counts<K>
where
    K: std::hash::Hash + Eq,
{
    fn counts<N>(self) -> HashMap<K, N>
    where
        N: AddAssign + Zero + One;
}

impl<K, I> Counts<K> for I
where
    K: std::hash::Hash + Eq,
    I: Iterator<Item = K>,
{
    fn counts<N>(self) -> HashMap<K, N>
    where
        N: AddAssign + Zero + One,
    {
        let mut hist = HashMap::new();
        for item in self {
            let count = hist.entry(item).or_insert_with(N::zero);
            *count += N::one();
        }
        hist
    }
}

fn line_to_coord(line: &str) -> (i64, i64) {
    let mut x = 0;
    let mut y = 0;
    for char in line.chars() {
        match char {
            'n' => y += 1,
            's' => y -= 1,
            'e' => x = if (x + y) % 2 == 0 { x + 2 } else { x + 1 },
            'w' => x = if (x + y) % 2 == 0 { x - 2 } else { x - 1 },
            _ => panic!("unhandled case {}", char),
        }
    }
    (x, y)
}

fn day24a(input: &str) -> usize {
    input
        .trim_end()
        .split('\n')
        .map(line_to_coord)
        .counts()
        .values()
        .filter(|x: &&i64| *x % 2 == 1)
        .count()
}

fn neighbor_addrs((x, y): &(i64, i64)) -> Vec<(i64, i64)> {
    vec![
        (x - 1, y + 1),
        (x + 1, y + 1),
        (x + 2, *y),
        (x + 1, y - 1),
        (x - 1, y - 1),
        (x - 2, *y),
    ]
}

fn step_ca(state: HashSet<(i64, i64)>) -> HashSet<(i64, i64)> {
    let neighbor_counts: HashMap<(i64, i64), usize> =
        state.iter().flat_map(neighbor_addrs).counts();
    let new_black_tiles = neighbor_counts
        .iter()
        .filter(|&(addr, count)| {
            if state.contains(addr) {
                (1..=2).contains(count)
            } else {
                2 == *count
            }
        })
        .map(|x| x.0);
    HashSet::from_iter(new_black_tiles.cloned())
}

fn day24b(input: &str) -> usize {
    let mut state: HashSet<(i64, i64)> = input
        .trim_end()
        .split('\n')
        .map(line_to_coord)
        .counts()
        .iter()
        .filter(|(_, n): &(&(i64, i64), &i64)| *n % 2 == 1)
        .map(|x| *x.0)
        .collect();
    for _ in 0..100 {
        state = step_ca(state);
    }
    state.len()
}

pub fn main() {
    let input = shared::puzzle_input!();
    let p1 = day24a(&input);
    let p2 = day24b(&input);
    println!("part 1: {}\npart 2: {}", p1, p2);
}
