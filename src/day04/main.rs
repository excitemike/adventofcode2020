use lazy_static::lazy_static;
use regex::Regex;
use std::collections::HashMap;

const REQ_KEYS: [&str; 7] = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"];
const VALID_ECLS: [&str; 7] = ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"];
lazy_static! {
    static ref HGT_REGEX: Regex = Regex::new("^(\\d+)((?:cm)|(?:in))$").unwrap();
    static ref HCL_REGEX: Regex = Regex::new("^#[0-9a-f]{6}$").unwrap();
    static ref PID_REGEX: Regex = Regex::new("^[0-9]{9}$").unwrap();
}

pub fn valid_hgt(hgt: &str) -> bool {
    if let Some(caps) = HGT_REGEX.captures(hgt) {
        if let Some(m) = caps.get(2) {
            match m.as_str() {
                "cm" => {
                    if let Some(m) = caps.get(1) {
                        let centimeters = m.as_str().parse::<u16>().unwrap();
                        return (150..=193).contains(&centimeters);
                    }
                }
                "in" => {
                    if let Some(m) = caps.get(1) {
                        let inches = m.as_str().parse::<u16>().unwrap();
                        return (59..=76).contains(&inches);
                    }
                }
                _ => {}
            }
        }
    }
    false
}

pub fn main() {
    let puzzle_input = shared::puzzle_input!();
    let data = puzzle_input
        .trim_end()
        .split("\n\n")
        .map(|passport_block| {
            passport_block
                .split_whitespace()
                .map(|kv| {
                    if let [k, v] = kv.split(':').collect::<Vec<_>>().as_slice() {
                        (*k, *v)
                    } else {
                        ("", "")
                    }
                })
                .collect::<HashMap<&str, &str>>()
        })
        .filter(|passport| REQ_KEYS.iter().all(|x| passport.contains_key(x)))
        .collect::<Vec<_>>();
    let p2 = data.iter().filter(|passport| {
        (1920..=2002).contains(&passport["byr"].parse::<u16>().unwrap())
            && (2010..=2020).contains(&passport["iyr"].parse::<u16>().unwrap())
            && (2020..=2030).contains(&passport["eyr"].parse::<u16>().unwrap())
            && valid_hgt(passport["hgt"])
            && (HCL_REGEX.is_match(passport["hcl"]))
            && VALID_ECLS.iter().any(|&x| x == passport["ecl"])
            && PID_REGEX.is_match(passport["pid"])
    });
    println!("part 1: {}\npart 2: {}", data.len(), p2.count())
}
