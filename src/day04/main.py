#----
import re
req_keys = ['byr','iyr','eyr','hgt','hcl','ecl','pid']
valid_ecls = 'amb blu brn gry grn hzl oth'.split()

def valid_hgt(hgt) -> bool:
    match = re.match('^(\\d+)((?:cm)|(?:in))$', hgt)
    if match:
        if match.group(2) == 'cm':
            return 150 <= int(match.group(1)) <= 193
        elif match.group(2) == 'in':
            return 59 <= int(match.group(1)) <= 76
    return False

def main(puzzle_input):
    data = puzzle_input.strip().split('\n\n')
    data = [[s2.split(':') for s2 in s1.split()] for s1 in data]
    data = [{k:v for [k,v] in kv} for kv in data]
    data = [passport for passport in data if all(key in passport for key in req_keys)]
    p2 = sum(1
        for passport in data
        if  (1920 <= int(passport['byr']) <= 2002)
        and (2010 <= int(passport['iyr']) <= 2020)
        and (2020 <= int(passport['eyr']) <= 2030)
        and valid_hgt(passport['hgt'])
        and (re.match('^#[0-9a-f]{6}$', passport['hcl']) is not None)
        and (passport['ecl'] in valid_ecls)
        and (re.match('^[0-9]{9}$', passport['pid']) is not None))
    print(f"part 1: {len(data)}\npart 2: {p2}")
#----

if __name__ == "__main__":
    import os
    import subprocess
    day = int(os.path.split(__file__)[0][-2:])
    main(subprocess.run(f'python src/shared/get_puzzle_input.py {day}'.split(), capture_output=True, text=True).stdout)
