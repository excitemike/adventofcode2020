def main(puzzle_input):
    # ---- day-specific code start
    def check_valid(candidate, numbers, i):
        for j, prior_candidate_a in enumerate(numbers[:i]):
            for prior_candidate_b in numbers[j+1:i]:
                if prior_candidate_a != prior_candidate_b and prior_candidate_a + prior_candidate_b == candidate:
                    return True
        return False

    def find_first_invalid(numbers):
        offset = 25
        for i, candidate in enumerate(numbers[offset:]):
            candidate = numbers[i+offset]
            if not check_valid(candidate, numbers, i+offset):
                return candidate

    numbers = [int(line) for line in puzzle_input.strip().splitlines()]
    p1 = find_first_invalid(numbers)

    def p2(numbers, p1):
        range_start = 0
        range_end = range_start + 2
        total = numbers[range_start] + numbers[range_start+1]
        while True:
            # too high? pop off the low end
            if total > p1:
                total -= numbers[range_start]
                range_start += 1
                # enforce minimum 2 items
                if range_end < range_start+2:
                    total += numbers[range_end]
                    range_end += 1
            # too low? add to the high end
            if total < p1:
                total += numbers[range_end]
                range_end += 1
            # done!
            if total == p1:
                return min(numbers[range_start:range_end]) + max(numbers[range_start:range_end])

    print(f"part 1: {p1}\npart 2: {p2(numbers, p1)}")
    # ---- day-specific code end

if __name__ == "__main__":
    import os
    import subprocess
    day = int(os.path.split(__file__)[0][-2:])
    main(subprocess.run(f'python src/shared/get_puzzle_input.py {day}'.split(), capture_output=True, text=True).stdout)

