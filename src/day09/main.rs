use std::cmp::Ordering;

fn find_first_invalid(xs: &[usize]) -> usize {
    let offset = 25;
    for (i, x) in xs[offset..].iter().enumerate() {
        if !is_valid(*x, xs, i + offset) {
            return *x;
        }
    }
    panic!()
}
fn is_valid(x: usize, xs: &[usize], i: usize) -> bool {
    for (j, a) in xs[..i].iter().enumerate() {
        for b in xs[j + 1..i].iter() {
            if (a != b) && (a + b == x) {
                return true;
            }
        }
    }
    false
}

fn p2(xs: &[usize], p1: &usize) -> usize {
    let mut range_start = 0usize;
    let mut range_end = range_start + 2;
    let mut total = xs[range_start] + xs[range_start + 1];
    loop {
        match total.cmp(p1) {
            // too high - pop off the bottom end
            Ordering::Greater => {
                total -= xs[range_start];
                range_start += 1;
                // enforce minimum 2 items
                if range_end < range_start + 2 {
                    total += xs[range_end];
                    range_end += 1;
                }
            }
            // too low - add more to the top
            Ordering::Less => {
                total += xs[range_end];
                range_end += 1;
            }
            // done
            Ordering::Equal => {
                let slice = &xs[range_start..range_end];
                return slice.iter().min().unwrap() + slice.iter().max().unwrap();
            }
        }
    }
}

pub fn main() {
    let puzzle_input = shared::puzzle_input!();
    let xs = puzzle_input
        .trim_end()
        .split('\n')
        .map(|x| x.parse::<usize>().unwrap())
        .collect::<Vec<_>>();
    let p1 = find_first_invalid(&xs);
    println!("part 1: {}\npart 2: {}", p1, p2(&xs, &p1));
}
