def main(puzzle_input):
    # ---- day-specific code start
    from functools import reduce

    blocks = [s.splitlines() for s in puzzle_input.strip().split("\n\n")]
    p1 = sum(len({c for line in block for c in line}) for block in blocks)
    p2 = sum(
        len(
            reduce(
                lambda a, b: a.intersection(b),
                [set(line) for line in block],
                set("abcdefghijklmnopqrstuvwxyz"),
            )
        )
        for block in blocks
    )
    print(f"part 1: {p1}\npart 2: {p2}")
    # ---- day-specific code end

if __name__ == "__main__":
    import os
    import subprocess
    day = int(os.path.split(__file__)[0][-2:])
    main(subprocess.run(f'python src/shared/get_puzzle_input.py {day}'.split(), capture_output=True, text=True).stdout)
