use std::collections::HashSet;
pub fn main() {
    let all_letters = ('a'..='z').collect::<HashSet<_>>();
    let puzzle_input = shared::puzzle_input!();
    let blocks = puzzle_input.trim_end().split("\n\n").collect::<Vec<_>>();
    let p1: usize = blocks
        .iter()
        .map(|&block| {
            block
                .split('\n')
                .flat_map(|s| s.chars())
                .collect::<HashSet<_>>()
                .len()
        })
        .sum();
    let p2: usize = blocks
        .iter()
        .map(|block| {
            block
                .split('\n')
                .map(|line| line.chars().collect::<HashSet<_>>())
                .fold(all_letters.clone(), |a, b| {
                    a.intersection(&b).cloned().collect::<HashSet<_>>()
                })
                .len()
        })
        .sum();
    println!("part 1: {}\npart 2: {}", p1, p2);
}
