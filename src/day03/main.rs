fn count_trees(data: &[Vec<bool>], step: &(usize, usize)) -> usize {
    data.iter()
        .enumerate()
        .filter(|(y, row)| (y % step.1 == 0) && row[((y / step.1) * step.0) % row.len()])
        .count()
}
pub fn main() {
    let puzzle_input = shared::puzzle_input!();
    let data = puzzle_input
        .trim_end()
        .split('\n')
        .map(|line| {
            line.trim_end()
                .chars()
                .map(|c| c == '#')
                .collect::<Vec<_>>()
        })
        .collect::<Vec<_>>();
    println!(
        "part 1: {}\npart 2: {}",
        count_trees(&data, &(3, 1)),
        [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)]
            .iter()
            .map(|step| count_trees(&data, step))
            .product::<usize>()
    )
}
