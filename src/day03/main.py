def main(puzzle_input):
    from functools import reduce
    p2steps = [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)]
    data = list(map(lambda s: list(map(lambda c: c=='#', s.strip())),
                    puzzle_input.strip().splitlines()))
    product = lambda l: reduce(lambda a, b: a*b, l, 1)
    def count_trees(stepx, stepy):
        return sum (1 for y, row in enumerate(data)
                    if (y % stepy == 0) and row[((y // stepy) * stepx) % len(row)])
    print(f'part 1:', count_trees(3, 1))
    print(f'part 2:', product(count_trees(*step) for step in p2steps))

if __name__ == "__main__":
    import os
    import subprocess
    day = int(os.path.split(__file__)[0][-2:])
    main(subprocess.run(f'python src/shared/get_puzzle_input.py {day}'.split(), capture_output=True, text=True).stdout)
