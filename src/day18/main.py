def main(puzzle_input):
    # ---- day-specific code start
    def evaluate(expr, precedence_map):
        values_stack, op_stack = [], []

        def do_op(op):
            nonlocal values_stack
            b, a = values_stack.pop(), values_stack.pop()
            if op == '+':
                values_stack.append(a+b)
            elif op == '*':
                values_stack.append(a*b)
            else:
                assert False
        for c in expr:
            if c.isspace():
                continue
            if c.isdigit():
                values_stack.append(int(c))
            elif c == '(':
                op_stack.append(c)
            elif c == ')':
                while op_stack[-1] != '(':
                    do_op(op_stack.pop())
                if op_stack[-1] == '(':
                    op_stack.pop()
            else:
                while op_stack and (op_stack[-1] != '(') and (precedence_map[op_stack[-1]] >= precedence_map[c]):
                    do_op(op_stack.pop())
                op_stack.append(c)
        while op_stack:
            do_op(op_stack.pop())
        assert len(values_stack) == 1
        return values_stack[0]

    p1, p2 = 0, 0
    for line in puzzle_input.strip().splitlines():
        p1 += evaluate(line, {'+': 1, '*': 1})
        p2 += evaluate(line, {'+': 2, '*': 1})

    print(f"part 1: {p1}\npart 2: {p2}")
    # ---- day-specific code start


if __name__ == "__main__":
    import os
    import subprocess
    day = int(os.path.split(__file__)[0][-2:])
    main(subprocess.run(
        f'python src/shared/get_puzzle_input.py {day}'.split(), capture_output=True, text=True).stdout)
