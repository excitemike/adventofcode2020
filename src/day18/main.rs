#![allow(unused_imports)]
use itertools::Itertools;
use unicode_segmentation::UnicodeSegmentation;

fn evaluate_p1<I: Iterator<Item = char>>(tokens: &mut I) -> usize {
    let mut values_stack = Vec::new();
    let mut ops_stack = Vec::new();
    while let Some(c) = tokens.next() {
        match c {
            '+' => ops_stack.push(c),
            '*' => ops_stack.push(c),
            '(' => values_stack.push(evaluate_p1(tokens)),
            ')' => {
                assert!(values_stack.len() == 1, "{:?}", values_stack);
                assert!(ops_stack.is_empty(), "{:?}", ops_stack);
                return values_stack.pop().unwrap();
            }
            _ => match c.to_digit(10) {
                Some(x) => values_stack.push(x as usize),
                None => panic!(),
            },
        }
        while (values_stack.len() > 1) && !ops_stack.is_empty() {
            do_adds(&mut values_stack, &mut ops_stack);
            do_multiplies(&mut values_stack, &mut ops_stack);
        }
    }
    assert!(values_stack.len() == 1);
    assert!(ops_stack.is_empty(), "{:?}", ops_stack);
    values_stack.pop().unwrap()
}

fn do_adds(values_stack: &mut Vec<usize>, ops_stack: &mut Vec<char>) {
    while values_stack.len() > 1 {
        if let Some(&'+') = ops_stack.last() {
            ops_stack.pop();
            let rhs = values_stack.pop().unwrap();
            let lhs = values_stack.pop().unwrap();
            values_stack.push(lhs + rhs);
        //println!("{} + {} = {}", lhs, rhs, values_stack.last().unwrap());
        } else {
            break;
        }
    }
}
fn do_multiplies(values_stack: &mut Vec<usize>, ops_stack: &mut Vec<char>) {
    while values_stack.len() > 1 {
        if let Some(&'*') = ops_stack.last() {
            ops_stack.pop();
            let rhs = values_stack.pop().unwrap();
            let lhs = values_stack.pop().unwrap();
            values_stack.push(lhs * rhs);
        //println!("{} * {} = {}", lhs, rhs, values_stack.last().unwrap());
        } else {
            break;
        }
    }
}

fn evaluate_p2<I: Iterator<Item = char>>(tokens: &mut I) -> usize {
    let mut values_stack = Vec::new();
    let mut ops_stack = Vec::new();
    while let Some(c) = tokens.next() {
        match c {
            '+' => ops_stack.push(c),
            '*' => {
                do_adds(&mut values_stack, &mut ops_stack);
                ops_stack.push(c)
            }
            '(' => values_stack.push(evaluate_p2(tokens)),
            ')' => break,
            _ => match c.to_digit(10) {
                Some(x) => values_stack.push(x as usize),
                None => panic!(),
            },
        }
    }
    do_adds(&mut values_stack, &mut ops_stack);
    do_multiplies(&mut values_stack, &mut ops_stack);
    assert!(values_stack.len() == 1, "{:?}", values_stack);
    assert!(ops_stack.is_empty(), "{:?}", ops_stack);
    values_stack.pop().unwrap()
}

pub fn main() {
    let input = shared::puzzle_input!();
    let mut parsed_input = input
        .trim_end()
        .split('\n')
        .map(|line| line.chars().filter(|c| !c.is_whitespace()).collect_vec())
        .collect_vec();

    let mut p1 = 0;
    for tokens in parsed_input.iter() {
        p1 += evaluate_p1(&mut tokens.iter().cloned());
    }

    let mut p2 = 0;
    for tokens in &mut parsed_input {
        p2 += evaluate_p2(&mut tokens.iter().cloned());
    }

    println!("part 1: {}\npart 2: {}", p1, p2);
}
